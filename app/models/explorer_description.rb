class ExplorerDescription < ActiveRecord::Base
  attr_accessible :name, :description, :version, :explorer_class_name, :uses_oauth
  serialize :mapping_keys

  has_many :explorers
  has_many :mappings

  validates :name, :presence => true
  validates :description, :presence => true

  def version_name
    return self.name + " (#{self.version}) "
  end

  def sample_file()
    "samples/json/#{sample_file_name}"
  end

  def benchmark_file()
    "samples/benchmarks/#{sample_file_name}"
  end

  def sample_file_name()
    explorer_class_name.underscore + ".json"
  end

  def build_explorer
    explorer = eval(explorer_class_name).new
    explorer.explorer_description = self
    return explorer
  end

  def standard_mapping()
    self.mappings.where(explorer_id: nil).first
  end
end
