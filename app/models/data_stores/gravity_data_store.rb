class GravityDataStore < Neo4jDataStore

  def create_jsdg
    has_more_nodes = true
    last_node_id = nil
    remove_existing_files()

    while(has_more_nodes)
      nodes = get_nodes(last_node_id, 10000).group_by{|n| n[1]}
      first_node_id = nodes.keys.first
      last_node_id = nodes.keys.last
      has_more_nodes = nodes.size == 10000

      x = Time.now
      relationships = get_relationships(first_node_id, last_node_id)
      info "writing relationships for #{relationships.size} nodes"
      relationships.each_pair do |source_id, rels|
        nodes[source_id].first[3] = rels.collect{|rel| rel[1..-1]}
      end
      info "written relationships in #{Time.now - x}s"

      info "writing to file: #{nodes.size} nodes between id(#{first_node_id}) and id(#{last_node_id})"
      x = Time.now
      File.open(graph_file, "ab") do |f|
        nodes.values.each do |node|
          f.info node.first.to_json
        end
      end
      info "Done writing to file in #{Time.now - x}s"
    end
  end

  def get_nodes(last_node_id, max_nodes)
    where = last_node_id.nil? ? "" : "WHERE id(n) > #{last_node_id}"
    query = "MATCH n #{where} RETURN labels(n), id(n), n"
    nodes = do_in_steps(query, 250, max_nodes)
    nodes.each do |node|
      node[0] = node[0].first
      node[2] = node[2].map.collect{|k,v| v.is_a?(String) ? [k,v[0..255]] : [k,v]}
      node[3] ||= []
    end
    return nodes
  end

  def get_relationships(from, to)
    where = "WHERE id(n) >= #{from} AND id(n) <= #{to}"
    do_in_steps("MATCH (n)-[r]->(t) #{where} RETURN id(n), type(r), id(t)", 2000).group_by{|res| res.first}
  end

  def nodes_between(upper, lower)
    do_in_steps("MATCH n WHERE id(n) >= #{upper} AND id(n) < #{lower} RETURN labels(n), id(n), n", 500)
  end

  def do_in_steps(query, limit = 500, max_nodes = Float::INFINITY)
    skip = 0
    results = []
    while((skip.modulo(limit) == 0) && (skip < max_nodes))
      q = query + " SKIP #{skip} LIMIT #{limit}"
      x = Time.now
      res = run_query(q)
      skip += res.size
      results.concat(res)
      info "round #{(skip/limit) + 1}. #{Time.now - x}. query: #{q}"
      break if res.empty?
    end
    info "Ran #{query} in #{Time.now - start}s, Results: #{results.size}"
    return results
  end

  def run_query(query)
    tx = neo.begin_transaction(query)
    neo.commit_transaction(tx)
    return tx["results"].first["data"].collect{|res| res["row"]}
  end

  def graph_file
    file_index = 0
    while(File.exist?(filename(file_index)) && ((File.size(filename(file_index)) / 2**20) > 1023))
      file_index += 1
    end
    filename(file_index)
  end

  def filename(file_index)
    Rails.root.join("public/graphs/#{explorer.mission.id}_graph.#{file_index}.jsdg")
  end

  def remove_existing_files
    FileUtils.rm_rf(Dir.glob(filename("*")), secure: true)
  end
end