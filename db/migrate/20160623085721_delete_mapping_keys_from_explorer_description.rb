class DeleteMappingKeysFromExplorerDescription < ActiveRecord::Migration
  def change
    remove_column :explorer_descriptions, :mapping_keys
  end
end
