function loadMapping(clickedEl){
  clickedEl.effect("highlight", {}, 2000);
  return $.ajax({
    url : clickedEl.parent().attr("data-update-url"),
    type: "GET",
    data : {key: clickedEl.attr("data-full-key")},
    dataType: 'json',
    success: function (json) {
      $(json).each(function(i, str){
        var targetEl = $("#" + str);
        targetEl.effect("highlight", {}, 2000);
        $(window).scrollTop(targetEl.offset().top);
      });
    },
    error: function (data) {
      insertNewMappingKey($(data.responseText), clickedEl);
    }
  });
};

function insertNewMappingKey(newElement, clickedEl){
  $("#mapping-list").children().each(function(i, el){
    if($(el).attr("data-full-key") > newElement.attr("data-full-key")){
      newElement.insertBefore($(el));
      return false;
    }
  });
  if(!document.contains(newElement[0])){
    $("#mapping-list").append(newElement);
  };
  newElement.effect("highlight", {}, 2000);
  $(window).scrollTop(newElement.offset().top);
};

function highlight_both(key){
  // highlight the mapping key
  $("div[data-full-key='" + key + "']").effect("highlight", {}, 2000);
  // and now highlight the according json element
  var targetElement = $("span[data-full-key='" + key + "']");
  if(targetElement.length == 0){
    targetElement = $("pre[class='json-view']");
  }
  targetElement.effect("highlight", {}, 2000);
  $(window).scrollTop(targetElement.offset().top);
};

var display_key_edit = function(el){
  $(el).hide();
  $(el).siblings("#mapping_key_key").show();
}

$(document).on("ajax:success", ".update-me", function(e, data, status, xhr) {
  var newDiv = $(data);
  $($(this).closest(".mapping-key")[0]).replaceWith(newDiv);
  newDiv.effect("highlight", {color: "#cfc"}, 2000);
});

$(document).on("ajax:success", ".show-preview-btn", function(e, data, status, xhr) {
  var preview_div = $(data);
  $("#" + preview_div.attr("id")).replaceWith(preview_div);
  preview_div.modal("show");
});

$(document).on("change", "form.edit_mapping_key", function(e, data, status, xhr) {
  $(this).submit();
});

$(document).on("click", "pre span", function(e, data, status, xhr){
  loadMapping($(this));
});