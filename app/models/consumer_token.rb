class ConsumerToken < ActiveRecord::Base
  
  attr_accessible :user_id, :provider
  
  # Modify this with class_name etc to match your application
  belongs_to :user

end