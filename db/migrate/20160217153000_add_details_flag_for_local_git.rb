class AddDetailsFlagForLocalGit < ActiveRecord::Migration
  def change
    add_column :explorers, :parse_details, :boolean, default: true
  end
end
