class GithubGitExplorer < GithubExplorer

  attr_accessible :branches, :parse_details

  def go_grabbing!
    commits = get_commit_info()
    info_msg("Retreived #{commits.size} commits")
    if parse_details
      slices = 0
      commits.each_slice(20) do |commit_shas|
        Sidekiq::Client.push(
          'queue' => mission.explorer_task_queue,
          'class' => GithubGitExplorerJob,
          'args' => [self.id, commit_shas, slices * 20]
        )
        slices += 1
      end
      info_msg("Enqueued #{slices} jobs for #{commits.size} commit details.")
    end
  end

  def get_commit_info()
    known_commits = Set.new
    # we start at the last run, the start data, or some bogus point in the past
    lower_limit = self.last_run || self.start_date || DateTime.parse("1983-10-11")
    upper_limit = self.end_date || DateTime.now()

    get_branches.collect do |branch_name|
      info_msg("Getting commits between #{lower_limit} and #{upper_limit} for branch: #{branch_name}")
      commits_in_branch = paginate{|options| client.commits_between(repo, lower_limit, upper_limit, branch_name, options)}
      info_msg("Found #{commits_in_branch.size} commits in branch.")
      known_commits.merge(commits_in_branch.collect{|commit| commit[:sha]})
    end

    info_msg("found a total of #{known_commits.size} commits")
    return known_commits
  end

  def get_branches
    available_branches = []

    each_page({}) do |options|
      client.branches(repo, options)
      available_branches.concat(last_results.collect{|res| res[:name]})
    end
    info_msg("#{available_branches.size} available branches: #{available_branches}")

    if branches.blank?
      return available_branches
    else
      target_branches = branches.split(",").collect{|branch_name| branch_name.strip.downcase}
      return available_branches.select{|branch_name| target_branches.include?(branch_name.downcase)}
    end
  end

  def get_commit_details(commits = [])
    commits.each_with_index do |sha, i|
      perform_request{|options| data_store << client.commit(repo, sha).to_attrs}
    end
  end

  class GithubGitExplorerJob
    include Sidekiq::Worker
    sidekiq_options :retry => true, :backtrace => true

    def perform(explorer_id, commit_shas, offset)
      explorer = Explorer.find(explorer_id)
      explorer.get_commit_details(commit_shas)
      explorer.info_msg("Commits #{offset}-#{offset + commit_shas.size} done.")
    end
  end
end
