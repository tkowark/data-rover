class GithubPullRequestExplorer < GithubIssueDetailsExplorer

  def go_grabbing!
    paginate(std_opts(:state => :all), false) do |opts|
      client.pull_requests(repo, opts)
      pull_requests = last_results()
      pull_requests.each{|pr| pr[:url] = pr[:issue_url]}
      data_store.concat(pull_requests)
      pr_infos = pull_requests.collect{|pr| {"url" => pr[:url], "number" => pr[:number]}}
      enqueue_job(pr_infos)
      info_msg("page: #{opts[:page] || 1}, results: #{data_store.size}")
    end
  end

  def get_details(issue_infos)
    details = issue_infos.collect do |issue_info|
      commits = []
      paginate(std_opts, false) do |opts|
        client.pull_request_commits(repo, issue_info["number"], opts)
        commits.concat(last_results)
      end
      comments = []
      paginate(std_opts, false) do |opts|
        client.pull_request_comments(repo, issue_info["number"], opts)
        comments.concat(last_results)
      end
      {"url" => issue_info["url"], "commits" => commits, "review_comments" => comments}
    end
    success_msg("Collected comments and commits for #{issue_infos.size} pull request.")
  end
end