class CreateMappingKeys < ActiveRecord::Migration
  def change
    create_table :mapping_keys do |t|
      t.string :key

      # basic attribute handling
      t.string :datatype
      t.text :primary_key
      t.string :type_name

      # movement and renaming
      t.string :move_to
      t.string :rename_to

      # foreing key handling
      t.string :target_type
      t.string :foreign_key

      # booleans
      t.boolean :is_regexp
      t.boolean :ignore
      t.boolean :no_updates
      t.boolean :no_inserts
      t.boolean :contains_links

      # foreign keys
      t.references :mapping
      t.timestamps
    end

    add_index :mapping_keys, :mapping_id
  end
end