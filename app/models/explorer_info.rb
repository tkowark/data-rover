class ExplorerInfo < ActiveRecord::Base
  attr_accessible :event_type, :message, :data_import
  belongs_to :explorer

  ERROR = "error"
  INFO = "info"
  SUCCESS = "success"
end
