module MissionsHelper
  def interval_options
    [[t(:always), Mission::ALWAYS], [t(:hourly), Mission::HOURLY], [t(:twice_a_day), Mission::TWICE_A_DAY], [t(:daily), Mission::DAILY]]
  end

  def sidekiq_options
    opts = Mission::SIDEKIQS.collect{|optn| [optn, optn]} + ["all"]
    options_for_select(opts)
  end
end
