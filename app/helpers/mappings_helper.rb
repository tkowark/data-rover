module MappingsHelper

  def field_for_mapping_option(attrib, val, f, mk)
    send("field_for_#{attrib}".to_sym, attrib, val, f, mk)
  end

  def field_for_ignore(attrib, val, f, mk)
    f.check_box(attrib, class: "mapping-checkbox")
  end

  def field_for_type_name(attrib, val, f, mk)
    f.text_field(attrib, class: "input-small")
  end

  def field_for_move_to(attrib, val, f, mk)
    f.select(attrib, options_for_select(mk.possible_move_targets,val),{},{class: "input-small"})
  end

  def field_for_primary_key(attrib, val, f, mk)
    f.select(attrib, options_for_select(mk.possible_primary_keys,val), {}, {class: "input-small"})
  end

  def field_for_datatype(attrib, val, f, mk)
    f.select(attrib, options_for_select(MappingKey::DATATYPES, val), {}, {class: "input-medium"})
  end

  def field_for_rename_to(attrib, val, f, mk)
    f.text_field(attrib, class: "input-small")
  end

  def field_for_foreign_key(attrib, val, f, mk)
    f.text_field(attrib, class: "input-small")
  end

  def field_for_no_updates(attrib, val, f, mk)
    f.check_box(attrib, class: "mapping-checkbox")
  end

  def field_for_no_inserts(attrib, val, f, mk)
    f.check_box(attrib, class: "mapping-checkbox")
  end
  
  def field_for_no_relation(attrib, val, f, mk)
    f.check_box(attrib, class: "mapping-checkbox")
  end  
end
