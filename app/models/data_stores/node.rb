# internal classes for storing stuff
class Node
  attr_accessor :primary_key, :type, :db_id, :update, :insert

  def initialize(type = nil, pk = nil, db_id = nil)
    @attributes = {}
    @relations = {}
    @update = true
    @insert = true
    @primary_key = pk
    @type = type
    @db_id = db_id
  end

  def primary_key_value
    @attributes[primary_key]
  end

  def add_attribute(key, value)
    @attributes[key] ||= value
  end

  def add_relation(rel)
    rel.source = self
    @relations[rel.name] ||= {}
    @relations[rel.name][rel.target.indexed_pk] = rel
  end

  def all_relations()
    @relations.collect{|name, target_hashes| target_hashes.values}.flatten
  end

  def get_attributes()
    return @attributes
  end

  def indexed_pk
    @indexed_pk ||= "#{type}_#{primary_key}_#{primary_key_value}"
  end

  def clean_attributes
    return @attributes.reject{|k,v| v.nil?}
  end

  def follow_subobjects?
    insert && update
  end

  def merge_with!(other_node)
    other_node.get_attributes.each_pair{|k, v| @attributes[k] ||= v}
    other_node.all_relations.each{|relation| add_relation(relation)}
    @update ||= other_node.update
    @insert ||= other_node.insert
  end

  def nicely_formatted()
    out = "Node: #{type} #{primary_key_value} (insert: #{insert}, update: #{update})\n"
    out << "Attributes:  \n"
    @attributes.each_pair{|k, v| out << " - #{k}: #{v}\n"}
    out << "Relations:  \n"
    all_relations.each{|r| out << r.nicely_formatted + "\n"}
    return out
  end

  def label_string
    attrib_str = @attributes.collect{|k, v| "#{crop_string(k)}: #{crop_string(v)}"}.join("\n")
    return "#{type}|#{attrib_str}"
  end

  def crop_string(str, max_length = 30)
    return str unless str.is_a?(String)
    shortened_str = if str.size < max_length
      str
    else
      "#{str[0..10]} [...] #{str[-10..-1]}"
    end
    return shortened_str.gsub(Regexp.new("{|}|<.*?>"), "")
  end
end