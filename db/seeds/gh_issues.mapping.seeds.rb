# issues
gh_issue_explorer_descr = ExplorerDescription.find_by_explorer_class_name_and_version("GithubIssuesExplorer", "v3.0")
raise "Could not find description for Github Issues Explorer - run rake db:seeds:explorers first" if gh_issue_explorer_descr.nil?
mapping = gh_issue_explorer_descr.standard_mapping() || gh_issue_explorer_descr.mappings.create()
mapping.update_attributes!(:default_key => "url", name: "Linked Mapping", description: "Creates a simple graph structure")
mapping.mapping_keys.destroy_all
mapping.mapping_keys.where(key: "").first_or_create.update_attributes(type_name: "GithubIssue")
mapping.mapping_keys.where(key: "user").first_or_create.update_attributes(type_name: "GithubUser")
mapping.mapping_keys.where(key: "pull_request").first_or_create.update_attributes(type_name: "GithubPullRequest")
mapping.mapping_keys.where(key: "labels").first_or_create.update_attributes(type_name: "GithubLabel")
mapping.mapping_keys.where(key: "assignee").first_or_create.update_attributes(type_name: "GithubUser")
mapping.mapping_keys.where(key: "milestone").first_or_create.update_attributes(type_name: "GithubMilestone")
mapping.mapping_keys.where(key: "milestone.creator").first_or_create.update_attributes(type_name: "GithubUser")
mapping.mapping_keys.where(key: "body").first_or_create.update_attributes(datatype: "longText")
mapping.mapping_keys.where(key: "_at$").first_or_create.update_attributes(is_regexp: true, datatype: "dateTime")

# issue_comments
gh_issue_explorer_descr = ExplorerDescription.find_by_explorer_class_name_and_version("GithubIssueCommentsExplorer", "v3.0")
raise "Could not find description for Github Issue Comments Explorer - run rake db:seeds:explorers first" if gh_issue_explorer_descr.nil?
mapping = gh_issue_explorer_descr.standard_mapping() || gh_issue_explorer_descr.mappings.create()
mapping.update_attributes!(:default_key => "url", name: "Linked Mapping", description: "Creates a simple graph structure")
mapping.mapping_keys.destroy_all
mapping.mapping_keys.where(key: "").first_or_create.update_attributes(type_name: "GithubIssueComment")
mapping.mapping_keys.where(key: "user").first_or_create.update_attributes(type_name: "GithubUser")
mapping.mapping_keys.where(key: "body").first_or_create.update_attributes(datatype: "longText")
mapping.mapping_keys.where(key: "^_links").first_or_create.update_attributes(is_regexp:true, ignore: true)
mapping.mapping_keys.where(key: "_at$").first_or_create.update_attributes(is_regexp: true, datatype: "dateTime")

# issue events
gh_issue_explorer_descr = ExplorerDescription.find_by_explorer_class_name_and_version("GithubIssueEventsExplorer", "v3.0")
raise "Could not find description for Github Issue Events Explorer - run rake db:seeds:explorers first" if gh_issue_explorer_descr.nil?
mapping = gh_issue_explorer_descr.standard_mapping() || gh_issue_explorer_descr.mappings.create()
mapping.update_attributes!(:default_key => "url", name: "Linked Mapping", description: "Creates a simple graph structure")
mapping.mapping_keys.destroy_all
mapping.mapping_keys.where(key: "").first_or_create.update_attributes(type_name: "GithubIssueEvent")
mapping.mapping_keys.where(key: "actor").first_or_create.update_attributes(type_name: "GithubUser")
mapping.mapping_keys.where(key: "issue.url").first_or_create.update_attributes(type_name: "GithubIssue", foreign_key: "url", rename_to: "issue", move_to: "")
mapping.mapping_keys.where(key: "issue").first_or_create.update_attributes(ignore: true)
mapping.mapping_keys.where(key: "issue.user").first_or_create.update_attributes(ignore: true)
mapping.mapping_keys.where(key: "issue.assignee").first_or_create.update_attributes(ignore: true)
mapping.mapping_keys.where(key: "issue.labels").first_or_create.update_attributes(ignore: true)
mapping.mapping_keys.where(key: "issue.milestone").first_or_create.update_attributes(ignore: true)
mapping.mapping_keys.where(key: "issue.milestone.creator").first_or_create.update_attributes(ignore: true)
mapping.mapping_keys.where(key: "issue.pull_request").first_or_create.update_attributes(ignore: true)
mapping.mapping_keys.where(key: "assignee").first_or_create.update_attributes(type_name: "GithubUser")
mapping.mapping_keys.where(key: "rename.from").first_or_create.update_attributes(move_to: "")
mapping.mapping_keys.where(key: "rename.to").first_or_create.update_attributes(move_to: "")
mapping.mapping_keys.where(key: "rename").first_or_create.update_attributes(ignore: true)
mapping.mapping_keys.where(key: "label").first_or_create.update_attributes(ignore: true)
mapping.mapping_keys.where(key: "milestone.title").first_or_create.update_attributes(move_to: "")
mapping.mapping_keys.where(key: "milestone").first_or_create.update_attributes(ignore: true)
mapping.mapping_keys.where(key: "_at").first_or_create.update_attributes(is_regexp: true, datatype: "dateTime")