class AddFieldsForLocalGitExplorer < ActiveRecord::Migration
  def change
    add_column :explorers, :repository_path, :string
    add_column :explorers, :get_patches, :boolean, default: true
  end
end
