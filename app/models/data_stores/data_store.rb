class DataStore

  attr_accessor :explorer, :mapping, :map_hash, :size, :node_lookup, :node_index, :nodes, :queue

  def initialize(mapping)
    @size = 0
    @mapping = mapping
    @map_hash = mapping.hashify
    @regexp_keys = map_hash.keys.select{|map_key| map_key.is_a?(Regexp)}
    @regexp_matches = {}
    @mapping_cache = {}
    @name_cache = {}
    @opt_in = mapping.opt_in
    init_node_caches
  end

  def init_node_caches
    @node_index = {}
    @nodes = []
  end

  # 'Factory'
  def self.for_explorer(explorer)
    ds_class = Object.const_get(explorer.mission.target_data_store.to_s.classify + "DataStore")
    return ds_class.new(explorer.mapping)
  end

  # interface for explorers
  def <<(a_hash)
    @size += 1
    store([a_hash])
  end

  def concat(an_enumerable)
    @size += an_enumerable.size
    store(an_enumerable)
  end

  # data storing
  def store(data)
    store_sample(data) if explorer.update_samples
    raise Exception.new("Invalid Mapping") unless mapping.is_valid?
    fn = store_data_in_tmp_file(data)
    create_import_job(fn)
  end

  def store_data_in_tmp_file(data)
    fh = tmp_file
    fn = "tmpdata/#{fh}.json"
    File.open(fn, "wb"){|f| f.puts Oj.dump(data)}
    zf = "tmpdata/#{fh}.zip"
    `cd #{Rails.root} && zip #{zf} #{fn}`
    FileUtils.rm(fn)
    return "tmpdata/#{fh}"
  end

  def store_to_database()
    upsert_nodes
    add_relationships
    disconnect()
  end

  def create_import_job(fn)
    Sidekiq::Client.push('queue' => queue, 'class' => DataImportJob, 'args' => [explorer.id, fn])
  end

  def load_json(filename)
    init_node_caches
    timed("JSON load") do
      Oj.load(File.read(filename)).each{|obj| transform_object(obj)}
      FileUtils.rm(filename)
    end
  end

  def load_zip(filename)
    `cd #{Rails.root} && unzip -o #{filename}.zip`
    load_json(filename + ".json")
  end

  def timed(msg, o_count = nil, *args)
    start_time = Time.now
    res = yield
    total_time = Time.now - start_time
    i_msg = "#{msg} (#{(total_time).round(3)}s)"
    i_msg << " #{(o_count/total_time).round(1)} ops/s" unless o_count.nil?
    info(i_msg)
    return res
  end

  def tmp_file
    "#{explorer.class.name.underscore}_#{explorer.id}_#{SecureRandom.uuid}"
  end

  # lazy getters
  def queue
    @queue ||= explorer.mission.data_import_queue
  end

  def explorer
    @explorer ||= mapping.explorer
  end

  # logging
  def info(msg)
    explorer.explorer_infos.create!(data_import: true, :event_type => ExplorerInfo::INFO, message: msg[0..1000])
  end

  def error(msg)
    explorer.explorer_infos.create!(data_import: true, :event_type => ExplorerInfo::ERROR, message: msg[0..1000])
  end

  # The interface for new datastores
  def initialize_db
    raise "implement database initialization for your data store."
  end

  def upsert_nodes
    raise "implement an upsert operation for all nodes in our in-memory graph. Needs to set the db_id property."
  end

  def find_by_type_and_attributes(type, attribute_hash, return_values)
    raise "implement a simple search function allowing to narrow stuff down by attributes but only return certain values"
  end

  def add_relationships()
    raise "implement how to insert a set of relations into the database"
  end

  # performs cleanup after every storage to the database
  def disconnect()
    return true
  end

  def all_relationships
    nodes.collect{|node| node.all_relations}.flatten
  end

  # feel free to implement this method if your data store creates duplicates
  def remove_duplicates()
    return true
  end

  # storing samples from the returned json
  def store_sample(data)
    sample = sample_json
    joined_sample = update_sample(sample, data)
    sample_string = JSON.pretty_generate(joined_sample)
    File.open(sample_input_file, "w+") do |f|
      f.flock(File::LOCK_EX)
      f.puts(sample_string)
    end
  end

  def load_benchmark()
    benchmark_json.each{|res| transform_object(res)}
  end

  def load_sample()
    transform_object(sample_json)
  end

  def update_sample(hash, data)
    data.each do |date|
      next unless date.is_a?(Hash)
      hash.merge!(date.stringify_keys) do |key, val1, val2|
        # super special case: arrays of hashes
        if val1.is_a?(Array) && val1.first.is_a?(Hash) && val2.is_a?(Array) && val2.first.is_a?(Hash)
          [update_sample(val1.first, val2)]
        elsif val1.is_a?(Hash) && val2.is_a?(Hash)
          update_sample(val1,[val2])
        else
          val1.blank? ? val2 : val1
        end
      end
    end
    return hash
  end

  def sample_json
    if File.exist?(sample_input_file)
      return JSON.load(File.open(sample_input_file).read) || {}
    else
      return {}
    end
  end

  def benchmark_json()
    JSON.load(File.open(benchmark_input_file).read)
  end

  def benchmark_input_file
    mapping.explorer_description.benchmark_file()
  end

  def sample_input_file()
    mapping.explorer_description.sample_file()
  end

  def sample_output_file
    fn = mapping.explorer_description.explorer_class_name.underscore
    fn += "_#{explorer.id}" unless explorer.nil?
    Rails.root.join("samples", "outputs", fn + ".txt")
  end

  def write_sample_output
    File.open(sample_output_file, "w+"){|f|
      grouped_nodes = nodes.group_by{|n| n.type}
      f.puts "Datatypes: " + grouped_nodes.collect{|t,ns| "#{t} (#{ns.size})"}.join(", ") + "\r\n"
      nodes.each do |node|
        f.puts node.nicely_formatted
        f.puts "\r\n"
      end
    }
  end

  # Object transformation according to the mapping
  def transform_object(json_obj, object_key = nil)
    @node_lookup = {} if object_key.nil?
    obj = prepare_object(object_key || "", json_obj)

    json_obj.each_pair do |key, value|
      cur_key = object_key.nil? ? "#{key}" : "#{object_key}.#{key}"
      parent = move_target(cur_key, object_key || "") || obj
      case value
        when Array then handle_array(cur_key, value, parent)
        when Hash then handle_hash(cur_key, value, parent)
        else handle_attribute(cur_key, value, parent)
      end
    end

    return add_node(obj)
  end

  def prepare_object(key, json_obj)
    return nil if ignore?(key) || !move_to(key).nil?
    obj = node_lookup[key] = Node.new(type(key), primary_key(key))
    obj.update = update?(key)
    obj.insert = insert?(key)
    return obj
  end

  def move_target(element_key, parent_key)
    target_key = move_to(element_key) || move_to(parent_key)
    target_key.nil? ? nil : (node_lookup[target_key] ||= Node.new())
  end

  def handle_array(key, value, parent)
    if value.first.is_a?(Hash)
      value.each{|val| handle_hash(key, val, parent)}
    else
      value.each{|val| handle_attribute(key, val, parent)}
    end
  end

  def handle_hash(key, value, parent)
    target_object = transform_object(value, key)
    unless target_object.nil? || parent.nil? || no_relation?(key)
      parent.add_relation(Relation.new(attribute_name(key), target_object))
    end
  end

  def handle_attribute(key, value, parent)
    return if parent.nil?

    if foreign_key(key)
      handle_foreign_key(key, value, parent)
    else
      attribute_name = attribute_name(key)
      unless ignore?(key) && parent.primary_key != attribute_name
        add_attribute(attribute_name, value, datatype(key), parent)
      end
    end
  end

  def handle_foreign_key(key, value, parent)
    return if value.nil?
    target = link_target(key, value)
    parent.add_relation(Relation.new(attribute_name(key), target))
  end

  def link_target(key, value)
    obj = Node.new(type(key), foreign_key(key))
    obj.add_attribute(foreign_key(key), value)
    obj.update = update?(key)
    obj.insert = insert?(key)
    return add_node(obj)
  end

  def add_node(node)
    return nil if node.nil?
    if node_index[node.indexed_pk].nil?
      node_index[node.indexed_pk] = node
      nodes << node
    else
      node_index[node.indexed_pk].merge_with!(node)
    end
    return node_index[node.indexed_pk]
  end

  # helpers that determine which nodes to store and to update
  def nodes_to_insert
    nodes.select{|n| n.db_id.blank? && n.insert}
  end

  def nodes_to_update
    nodes.select{|n| !n.db_id.blank? && n.update}
  end

  # helpers for navigating through the mapping structures
  def primary_key(key)
    pk = get_mapping(key)[:primary_key] || map_hash[:default_key]
    attribute_name(key) == pk ? pk : attribute_name(pk)
  end

  def attribute_name(key)
    @name_cache[key] ||= get_attribute_name(key)
  end

  def get_attribute_name(key)
    map = get_mapping(key)
    map[:rename_to] || (map[:move_to].nil? ? key.split(".").last : key)
  end

  def type(key)
    get_mapping(key)[:type_name]
  end

  def move_to(key)
    get_mapping(key)[:move_to]
  end

  def ignore?(keys)
    map = get_mapping(keys)
    if map[:fake] == true
      return @opt_in == true
    else
      return map[:ignore] == true
    end
  end

  def datatype(keys)
    get_mapping(keys)[:datatype]
  end

  def foreign_key(keys)
    get_mapping(keys)[:foreign_key]
  end

  def update?(keys)
    get_mapping(keys)[:no_updates] != true
  end

  def insert?(keys)
    get_mapping(keys)[:no_inserts] != true
  end

  def no_relation?(keys)
    get_mapping(keys)[:no_relation] == true
  end

  def add_attribute(name, value, datatype, parent)
    parent.add_attribute(name, value)
  end

  def get_mapping(key)
    @mapping_cache[key] ||= find_mapping(key)
  end

  def find_mapping(key)
    # the straightforward case -> there is an explicit mapping
    return map_hash[key] if map_hash.has_key?(key)

    # if not, we check for regular expression matches
    alt_key = regexp_match(key)
    return map_hash[alt_key] if map_hash.has_key?(alt_key)

    # finally, just return an empty mapping.
    # the whole lookup magic is really hard to grasp because it is not explicitly visible in the frontend ...
    return {fake: true}
  end

  def regexp_match(key)
    @regexp_matches[key] ||= @regexp_keys.find{|regexp| key.match(regexp)} || true
  end
end