class DataImportJob

  include Sidekiq::Worker
  sidekiq_options :backtrace => true

  def perform(explorer_id, filename)
    explorer = Explorer.find(explorer_id)
    ds = DataStore.for_explorer(explorer)

    start = Time.now
    if !File.exist?(filename + ".zip")
      ds.error("Skipping Job because #{filename + ".zip"} does not exist.")
      return
    end
    ds.load_zip(filename)
    ds.store_to_database()
    FileUtils.rm(filename + ".zip")
  end
end