class ChangeUserClassToSearchClass < ActiveRecord::Migration
  def change
    rename_column :explorers, :user_class, :search_class
  end
end
