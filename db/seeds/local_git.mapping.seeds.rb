# Stackoverflow Explorer
so_explorer_descr = ExplorerDescription.find_by_explorer_class_name_and_version("LocalGitExplorer", "v1.0")
raise "Could not find description for Local Git Explorer - run rake db:seeds:explorers first" if so_explorer_descr.nil?
mapping = so_explorer_descr.standard_mapping() || so_explorer_descr.mappings.create()
mapping.update_attributes!(:default_key => "sha", name: "Linked Mapping", description: "Creates a simple graph structure")
mapping.mapping_keys.destroy_all
mapping.mapping_keys.where(key: "").first_or_create.update_attributes(type_name: "Commit")
mapping.mapping_keys.where(key: "_at$").first_or_create.update_attributes(datatype: "dateTime", is_regexp: true)
mapping.mapping_keys.where(key: "committer").first_or_create.update_attributes(type_name: "User", primary_key: "email", no_updates: true)
mapping.mapping_keys.where(key: "author").first_or_create.update_attributes(type_name: "User", primary_key: "email", no_updates: true)
mapping.mapping_keys.where(key: "files").first_or_create.update_attributes(type_name: "FileChange", primary_key: "file_sha", no_updates: true)
mapping.mapping_keys.where(key: "parents").first_or_create.update_attributes(type_name: "Commit", no_updates: true)
mapping.mapping_keys.where(key: "files.patch").first_or_create.update_attributes(datatype: "longText")