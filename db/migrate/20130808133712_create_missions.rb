class CreateMissions < ActiveRecord::Migration
  def change
    create_table :missions do |t|
      t.datetime :last_run
      t.references :user
      t.integer :interval
      t.string :name
      t.text :description
      t.string :database_name
      t.string :target_server
      t.string :username
      t.string :password
      t.integer :target_data_store_cd
      t.boolean :active, :default => false
      t.timestamps
    end

    add_index :missions, :user_id
  end
end
