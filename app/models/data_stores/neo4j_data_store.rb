class Neo4jDataStore < DataStore

  attr_accessor :neo

  # perform necessary initializations based on a sample. Just do, what you think is necessary
  def initialize_db
    load_sample
    nodes.each{|node| neo.execute_query("CREATE INDEX ON :#{node.type}(#{node.primary_key})")}
  end

  # insert previously unknown nodes
  def upsert_nodes
    timed("upserting #{nodes.size} nodes", nodes.size){
      i_batch = nodes.collect{|node| [:execute_query, upsert_node_query(node), node.clean_attributes]}
      batch(i_batch, 100){|res, i| nodes[i].db_id = res["body"]["data"].first.first.to_i}
    }
  end

  def add_relationships()
    relations = all_relationships()
    timed("adding #{relations.size} relations", relations.size){
      rel_batch = relations.collect{|relation| [:execute_query, relation_query(relation), relation_hash(relation)]}
      batch(rel_batch, 500)
    }
  end

  def upsert_node_query(node)
    query = "MERGE (n:#{node.type}{#{node.primary_key}:{`#{node.primary_key}`}})"
    attrib_setter = node.clean_attributes.collect{|k,v| "n.`#{k}` = {`#{k}`}"}.join(", ")
    return query + " ON CREATE SET #{attrib_setter} ON MATCH SET #{attrib_setter} RETURN id(n)"
  end

  def find_query(type, a_hash, return_values = [])
    value_attribs = a_hash.select{|at, v| !v.nil?}.collect{|k,v| "n.#{k} #{v.is_a?(Array) ? "IN" : "="} {#{k}}"}
    nil_attribs = a_hash.select{|at, v| v.nil?}.collect{|k,v| "n.#{k} IS NULL"}
    all_attribs = (value_attribs + nil_attribs).join(" AND ")
    returns = (return_values.collect{|rv| "n.#{rv}"} + ["id(n)"]).join(", ")
    q = "MATCH (n:#{type})"
    q += " WHERE #{all_attribs}" unless all_attribs.blank?
    q + " RETURN #{returns}"
  end

  def relation_query(relation)
    "MATCH n1,n2 WHERE id(n1) = {source_id} AND id(n2) = {target_id} MERGE (n1)-[:#{relation.name}]->(n2)"
  end

  def relation_hash(relation)
    {source_id: relation.source.db_id, target_id: relation.target.db_id}
  end

  # performs a batch in chunks of batch_size. any blocks will not notice that since the index is tempered...
  def batch(collection, batch_size = 1000, &block)
    runs = collection.size / batch_size
    runs += 1 if collection.size.modulo(batch_size) != 0

    runs.times do |run|
      cur_collection = collection[(run*batch_size)..(((run+1)*batch_size)-1)]
      run_batch(cur_collection, run, batch_size, &block)
    end
  end

  def run_batch(collection, run, batch_size, offset = 0, &block)
    begin
      unless block.nil?
        neo.batch_no_streaming(*collection).each_with_index do |res, i|
          block.call(res, i + offset + run * batch_size)
        end
      else
        neo.batch_no_streaming(*collection)
      end
    rescue Exception => e
      raise e if collection.size < 2
      collection.each.with_index{|el, i| run_batch([el], run, batch_size, i, &block)}
    end
  end

  def neo
    if @neo.nil?
      uri = URI.parse(explorer.mission.target_server)
      neo_opts = {:server => uri.host, port: uri.port, protocol: uri.scheme, :authentication => 'basic'}
      neo_opts[:username] = explorer.mission.username unless explorer.mission.username.blank?
      neo_opts[:password] = explorer.mission.password unless explorer.mission.password.blank?
      @neo = Neography::Rest.new(neo_opts)
    end
    @neo
  end

  def add_attribute(name, value, datatype, parent)
    if datatype == "dateTime" && !value.nil?
      convert_dateTime(value, name).each_pair{|k,v| parent.add_attribute(k,v)}
    else
      super
    end
  end

  def convert_dateTime(value, key)
    dt = case value
      when String then DateTime.parse(value)
      when Integer then Time.at(value).to_datetime
      when Time then value.to_datetime
      else value
    end

    res = Hash[["year","day","month","wday","hour","minute"].collect do |meth|
      [key + "_#{meth}", dt.send(meth.to_sym)]
    end]
    res[key] = dt.to_i
    return res
  end

  def find_by_type_and_attributes(type, attribute_hash, return_values)
    query = find_query(type, attribute_hash, return_values)
    tx = neo.begin_transaction([query, attribute_hash])
    raise tx["errors"].first["message"] unless tx["errors"].empty?
    neo.commit_transaction(tx)
    return tx["results"].first["data"].collect do |r|
      Hash[ r["row"].collect.with_index{|val, i| [return_values[i] || tx["results"].first["columns"][i], val]}]
    end
  end
end