class ExplorersController < ApplicationController

  before_filter :authenticate_user!

  def new
    @description = ExplorerDescription.find(params[:explorer_description_id])
    @explorer = @description.build_explorer
    @explorer.mission = Mission.find(params[:mission_id])
  end

  def create
    @description = ExplorerDescription.find(params[:explorer].delete(:explorer_description_id))
    @mission = Mission.find(params[:explorer].delete(:mission_id))
    @explorer = @description.build_explorer
    @explorer.update_attributes(params[:explorer])
    @mission.explorers << @explorer
    if @explorer.save
      redirect_to @explorer.mapping
    else
      render :new
    end
  end

  def edit
    @explorer = Explorer.find(params[:id])
  end

  def update
    @explorer = Explorer.find(params[:id])
    respond_to do |format|
      params[:explorer].delete(:mission_id)
      params[:explorer].delete(:explorer_description_id)
      if @explorer.update_attributes(params[:explorer])
        format.html { redirect_to @explorer.mission, :notice => 'Explorer was successfully updated.' }
      else
        format.html { redirect_to @explorer.mission, :alert => 'Explorer could not be updated.' }
      end
    end
  end

  def destroy
    @explorer = Explorer.find(params[:id])
    respond_to do |format|
      if @explorer.destroy
        format.html { redirect_to @explorer.mission, :notice => 'Explorer was successfully deleted.' }
      else
        format.html { redirect_to @explorer.mission, :alert => 'Explorer could not be deleted.' }
      end
    end
  end

  def run_now
    @explorer = Explorer.find(params[:explorer_id])
    enqueued = @explorer.enqueue!
    respond_to do |format|
      if enqueued
        format.html { redirect_to @explorer.mission, :notice => "Explorer has been enqueued for parsing."}
      else
        format.html { redirect_to @explorer.mission, :alert => "Explorer already running, could not be enqueued for parsing."}
      end
    end
  end
end