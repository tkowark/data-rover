# Stackoverflow Explorer
so_explorer_descr = ExplorerDescription.find_by_explorer_class_name_and_version("StackoverflowExplorer", "v2.1")
raise "Could not find description for Stackoverflow Explorer - run rake db:seeds:explorers first" if so_explorer_descr.nil?
mapping = so_explorer_descr.standard_mapping() || so_explorer_descr.mappings.create()
mapping.update_attributes!(:default_key => "link", name: "Linked Mapping", description: "Creates a simple graph structure")
mapping.mapping_keys.destroy_all
mapping.mapping_keys.where(key: "").first_or_create.update_attributes(type_name: "StackoverflowQuestion")
mapping.mapping_keys.where(key: "owner$").first_or_create.update_attributes(type_name: "StackoverflowUser", is_regexp: true)
mapping.mapping_keys.where(key: "reply_to_user$").first_or_create.update_attributes(type_name: "StackoverflowUser", is_regexp: true)
mapping.mapping_keys.where(key: "comments$").first_or_create.update_attributes(type_name: "StackoverflowComment", is_regexp: true)
mapping.mapping_keys.where(key: "answers").first_or_create.update_attributes(type_name: "StackoverflowAnswer")
mapping.mapping_keys.where(key: "^notice").first_or_create.update_attributes(move_to: "", is_regexp: true)
mapping.mapping_keys.where(key: "display_name$").first_or_create.update_attributes(type_name: "GithubUser", is_regexp: true, foreign_key: "login", rename_to: "same_as", no_inserts: true)
mapping.mapping_keys.where(key: "^migrated_").first_or_create.update_attributes(is_regexp: true, ignore: true)