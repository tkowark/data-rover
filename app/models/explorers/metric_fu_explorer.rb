require 'open3'

class MetricFuExplorer < Explorer
  attr_accessible :project_name, :project_owner

  after_save :clean_repository_path!
  before_destroy :remove_repository_data!

  def go_grabbing!
    get_metrics_for_each_revision!
  end

  def clean_repository_path!
    unless self.repository_path.nil?
      self.update_attributes(:repository_path => self.repository_path + "/") unless self.repository_path.end_with?("/")
    end
  end

  def remove_repository_data!
    FileUtils.rm_rf(self.explorer_tmp_dir)
  end

  def mapping
    {
      :_default_keys => ["url"],
      "" => {:_type => "GithubCommit"},
      "files" => {:_type => "GithubFileChange", :_pk => ["raw_url"]}
    }
  end

  def get_metrics_for_each_revision!
    prepare_directory_structure!()
    run_in_repository("git checkout -- .")
    run_in_repository("git checkout master")
    run_in_repository("git pull")
    commits = get_commits_from_all_branches

    info_msg("Total commit count: #{commits.size}")
    to_do = commits.select{|commit_sha,already_parsed| !already_parsed}

    existing_info = commits.select{|commit_sha,already_parsed| already_parsed}.collect do |commit_oid, ap|
      JSON.parse(File.open(metrics_json_file(commit_oid)).read)
    end.each_slice(50) do |slice|
      data_store.concat(slice)
    end

    info_msg("Still to do: #{to_do.size} commits...")

    # generating the metrics takes so long, just uplaod it, alread...
    to_do.keys.each_with_index do |commit_oid, i|
      info_msg("creating new metrics file for commit #{commit_oid}. (#{i+1} / #{to_do.size})")
      metrics = create_metric_file(commit_oid)
      data_store << metrics unless metrics.nil?
    end
  end

  def get_commits_from_all_branches
    repo = Rugged::Repository.new(repo_dir)
    commits = {}
    repo.branches.each do |branch|
      next if branch.target.is_a?(Rugged::Reference)
      puts "getting commit from #{branch.name}"
      cleanup_application()
      repo.status{|file, stat| puts "#{file} is #{stat.inspect}"}
      repo.checkout(branch)
      repo.walk(repo.last_commit) do |commit|
        commits[commit.oid] = File.exist?(metrics_json_file(commit.oid)) ? true : false
      end
    end
    return commits
  end

  def create_metric_file(commit_oid)
    cleanup_application()
    run_in_repository("git checkout #{commit_oid}")
    info_msg("Checked out commit #{commit_oid}")
    unless is_rails_app?
      info_msg("Skipping commit #{commit_oid} as there is no rails app")
      return nil
    end

    runs = prepare_application()
    # tests can only run, if the app runs...otherwise, there simply will be no coverage data
    if runs && has_specs?
      tests_pass = run_in_repository("rake spec")
      info_msg("Ran tests in rev. #{commit_oid}, success: #{tests_pass}")
      run_in_repository("metric_fu --format yaml --out #{metrics_file(commit_oid)}")
    else
      tests_pass = false
      info_msg("App runs: #{runs}, has specs: #{has_specs?}")
      run_in_repository("metric_fu --format yaml --out #{metrics_file(commit_oid)} --no-rcov")
    end

    metrics = simple_metrics(commit_oid, tests_pass, runs)
    info_msg("gathered stats for application #{self.project_name} in rev. #{commit_oid}. tests pass? #{tests_pass}")
    return metrics
  end

  def is_rails_app?
    return File.exist?(repo_dir + "/Gemfile") && Dir.exist?(repo_dir + "/app") && Dir.exist?(repo_dir + "/config")
  end

  def prepare_application()
    copy_files
    add_metric_fu_and_simplecov_to_gemfile
    adapt_spec_helpers if has_specs?
    rv = determine_ruby_version()
    unless rv.nil?
      installed_ruby = run_in_repository("rvm install #{rv}")
      info_msg("installed ruby #{rv}, success: #{installed_ruby}")
    end
    bundles = run_in_repository("bundle")
    info_msg("Bundled application, success: #{bundles}")
    migrates = run_in_repository("rake db:drop db:create db:migrate RAILS_ENV=test")
    info_msg("Migrated application, success: #{migrates}")
    return bundles && migrates
  end

  def copy_files
    FileUtils.cp(dot_metrics, repo_dir + "/.metrics")
    FileUtils.cp(dot_simplecov, repo_dir + "/.simplecov")
    FileUtils.cp(database_yml, repo_dir + "/config/database.yml")
  end

  def add_metric_fu_and_simplecov_to_gemfile
    add_new_and_remove_lines_containing(
      repo_dir + "/Gemfile",
      ["gem \"simplecov\", :require => false", "gem \"metric_fu\", :require => false"],
      ["simplecov", "metric_fu"]
    )
  end

  def adapt_spec_helpers
    add_new_and_remove_lines_containing(repo_spec_helper, ["require 'simplecov'"], ["simplecov", "codeclimate"])
    add_new_and_remove_lines_containing(repo_rails_helper, [], ["simplecov", "codeclimate"])
  end

  def add_new_and_remove_lines_containing(filename, lines_to_add, blacklist_strings)
    return unless File.exist?(filename)
    new_content = lines_to_add
    File.open(filename, "r") do |file|
      File.foreach(file) do |line|
        new_content << line unless blacklist_strings.any?{|bs| line.downcase.include?(bs)}
      end
    end
    File.open(filename, "w+"){|f| f.puts(new_content)}
  end

  def determine_ruby_version
    gf = File.open(repo_dir + "/Gemfile", "r").read
    rv = gf.scan(/ruby \"(.*?)\"/).flatten.first
    if rv.nil? & File.exist?(repo_dir + "/.ruby-version")
      rv = File.open(repo_dir + "/.ruby-version").read.strip
    end
    return rv
  end

  def run_in_repository(cmd)
    Bundler.with_clean_env do
      stdin, stdout, stderr, wait_thr = Open3.popen3("bash -l -c 'cd #{repo_dir}; #{cmd}'")
      output = stdout.gets(nil)
      stdout.close
      errors = stderr.gets(nil)
      stderr.close
      exit_code = wait_thr.value

      if exit_code =! 0
        if ["rake", "bundle", "metric_fu"].any?{|whity| cmd.start_with?(whity)}
          error_msg("error in #{cmd}: #{errors}")
          return false
        else
          raise "exception in #{cmd}"
        end
      end

      return true
    end
  end

  def simple_metrics(commit_oid, tests_pass, app_runs)
    commit_stats = {
      :sha => commit_oid,
      :url => commit_url(commit_oid),
      :tests_pass => tests_pass,
      :app_runs => app_runs,
      :files => {}
    }

    return commit_stats unless File.exist?(metrics_file(commit_oid))

    metrics = YAML::load(File.open(metrics_file(commit_oid)).read)
    commit_stats[:total_complexity] = metrics[:flog][:total].round(2)
    commit_stats[:average_complexity] = metrics[:flog][:average].round(2)

    metrics[:flog][:method_containers].each do |method|
      commit_stats[:files][method[:path]] = {
        :filename => method[:path],
        :total_score => method[:total_score].round(2),
        :highest_score => method[:highest_score].round(2),
        :average_score => method[:average_score].round(2),
        :raw_url => file_url(commit_oid, method[:path])
      }
    end

    unless metrics[:rcov].nil?
      metrics[:rcov].delete("./.metrics")
      commit_stats[:test_coverage] = metrics[:rcov].delete(:global_percent_run)
      metrics[:rcov].each_pair do |file, file_stats|
        filename = file[2..-1]
        commit_stats[:files][filename] ||= {:raw_url => file_url(commit_oid, filename)}
        commit_stats[:files][filename][:test_coverage] = file_stats[:percent_run]
      end
    end

    commit_stats[:files] = commit_stats[:files].values
    File.open(metrics_json_file(commit_oid),"w+"){|f| f.write(commit_stats.to_json)}
    FileUtils.rm(metrics_file(commit_oid))
    return commit_stats
  end

  def file_url(commit_oid, filename)
    return "https://github.com/#{self.project_owner}/#{self.project_name}/raw/#{commit_oid}/#{filename}"
  end

  def commit_url(commit_oid)
    return "https://api.github.com/repos/#{self.project_owner}/#{self.project_name}/commits/#{commit_oid}"
  end

  def cleanup_application()
    run_in_repository("git checkout -- .")
    run_in_repository("git clean  -d  -fx \"\"")
    run_in_repository("git reset --hard")
    FileUtils.rm_rf(repo_dir + "coverage/")
  end

  def prepare_directory_structure!()
    [explorer_tmp_dir, repos_dir, stats_dir, repo_stats_dir].each do |dir|
      unless Dir.exist?(dir)
        Dir.mkdir(dir)
      end
    end
    create_custom_database_yml!
    clone_github_repository!
  end

  def create_custom_database_yml!
    db_yml = File.open(Rails.root.join("config", "database.yml")).read
    db_yml.gsub!(/database: .*$/, "database: #{self.project_name}")
    File.open(database_yml, "w+"){|f| f.puts db_yml}
  end

  def clone_github_repository!
    unless Dir.exist?(repo_dir)
      Bundler.with_clean_env do
        Dir.chdir(repos_dir) do
          unless system("git clone https://github.com/#{self.project_owner}/#{self.project_name}.git")
            FileUtils.rm_rf(repo_dir)
            raise "could not clone repository"
          else
            info_msg("Successfully cloned repository #{self.project_owner}/#{self.project_name}")
          end
        end
      end
    end
  end

  def has_specs?
    File.exist?(repo_spec_helper)
  end

  def repo_spec_helper
    repo_dir + "/spec/spec_helper.rb"
  end

  def repo_rails_helper
    repo_dir + "/spec/rails_helper.rb"
  end

  def dot_metrics()
    metric_fu_dir + "/" + "dot_metrics"
  end

  def dot_simplecov()
    metric_fu_dir + "/" + "dot_simplecov"
  end

  def database_yml()
    explorer_tmp_dir + "/" + "#{self.project_owner}_#{self.project_name}_database.yml"
  end

  def metric_fu_dir
    Rails.root.join("tmpdata", "metric_fu").to_s
  end

  def explorer_tmp_dir
    metric_fu_dir + "/#{self.id}"
  end

  def repos_dir
    explorer_tmp_dir + "/repos"
  end

  def stats_dir
    explorer_tmp_dir + "/stats"
  end

  def repo_stats_dir
    stats_dir + "/" + self.project_name
  end

  def repo_dir
    repos_dir + "/" + self.project_name
  end

  def metrics_file(commit_oid)
    repo_stats_dir + "/" + commit_oid + ".yml"
  end

  def metrics_json_file(commit_oid)
    repo_stats_dir + "/" + commit_oid + ".json"
  end
end
