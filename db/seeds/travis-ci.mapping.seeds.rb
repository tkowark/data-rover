# Travis-CI Explorer
travis_ci_explorer_descr = ExplorerDescription.find_by_explorer_class_name_and_version("TravisCiExplorer", "v1.0")
raise "Could not find description for Travis CI Explorer - run rake db:seeds:explorers first" if travis_ci_explorer_descr.nil?
# issues
mapping = travis_ci_explorer_descr.standard_mapping() || travis_ci_explorer_descr.mappings.create()
mapping.update_attributes!(:default_key => "url", name: "Linked Mapping", description: "Creates a simple graph structure", opt_in: true)
mapping.mapping_keys.destroy_all
mapping.mapping_keys.where(key: "").first_or_create.update_attributes(type_name: "TravisBuild")
mapping.mapping_keys.where(key: "url").first_or_create.update_attributes(datatype: "string")
mapping.mapping_keys.where(key: "repository_id").first_or_create.update_attributes(datatype: "integer")
mapping.mapping_keys.where(key: "duration").first_or_create.update_attributes(datatype: "integer")
mapping.mapping_keys.where(key: "number").first_or_create.update_attributes(datatype: "string")
mapping.mapping_keys.where(key: "state").first_or_create.update_attributes(datatype: "string")
mapping.mapping_keys.where(key: "_at").first_or_create.update_attributes(is_regexp: true, datatype: "dateTime")
mapping.mapping_keys.where(key: "commit.url").first_or_create.update_attributes(foreign_key: "url", rename_to: "commit", type_name: "GithubCommit", move_to: "")

travis_ci_explorer_descr = ExplorerDescription.find_by_explorer_class_name_and_version("TravisCiExplorer", "v1.0")
raise "Could not find description for Travis CI Explorer - run rake db:seeds:explorers first" if travis_ci_explorer_descr.nil?
# issues
mapping = travis_ci_explorer_descr.mappings.create()
mapping.update_attributes!(:default_key => "url", name: "Flat Mapping", description: "Creates a flat object", opt_in: true)
mapping.mapping_keys.destroy_all
mapping.mapping_keys.where(key: "duration").first_or_create.update_attributes(datatype: "integer", move_to: "commit", rename_to: "build_duration")
mapping.mapping_keys.where(key: "number").first_or_create.update_attributes(datatype: "string", move_to: "commit", rename_to: "build_number")
mapping.mapping_keys.where(key: "state").first_or_create.update_attributes(datatype: "string", move_to: "commit", rename_to: "build_state")
mapping.mapping_keys.where(key: "commit.url").first_or_create.update_attributes(datatype: "string")
mapping.mapping_keys.where(key: "commit").first_or_create.update_attributes(type_name: "GithubCommit")
