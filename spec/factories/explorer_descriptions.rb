FactoryGirl.define do
  factory :explorer_description do
    name "Test Description"
    description "Description of Test Description"
  end
end