require 'csv'

class PeopleExplorer < Explorer
	attr_accessor :client, :people_cache, :search_page
  attr_accessible :username, :password, :search_attribute, :search_class, :url, :csv
  has_attached_file :csv
  validates_attachment_content_type :csv, content_type: ["text/plain", "application/vnd.ms-excel", "text/comma-separated-values","text/csv"]
  Paperclip.options[:content_type_mappings] = {csv: "text/plain"}

  def go_grabbing!
    if csv.path.nil?
      usernames = query_usernames
      Sidekiq::Client.push(
        'queue' => mission.explorer_task_queue,
        'class' => PeopleSearchExplorerJob,
        'args' => [self.id, usernames]
      )
      info_msg("Created Search Job for #{usernames.size} users.")
    else
      search_for(csv_usernames)
    end
  end

  def csv_usernames
    csv_data = CSV.read(csv.path)
    csv_data.shift
    return csv_data.flatten.reject{|un| un.blank?}
  end

  def query_usernames
    data_store.find_by_type_and_attributes(search_class, {"LOCATION": nil}, [search_attribute]).collect{|row| row[search_attribute]}
  end

  def search_for(usernames)
    usernames.each.with_index do |user_id, i|
      result = nil
      search_strings(user_id).each do |search_string|
        result = search_for_user(search_string, user_id)
        break unless result.nil?
      end

      info_msg("Found #{result.nil? ? 'no' : ''} result for #{user_id}. #{i+1}/#{usernames.size}")
      unless result.nil? 
        sleep_interval = rand(4..10)
        info_msg("Going to sleep for #{sleep_interval}s")
        sleep(sleep_interval)
      end
    end
  end

  def search_strings(user_id)
    searches = [user_id]
    name = user_id.split("@").first
    searches << name if !name.nil? && name.match(/^[a-z]\d+/i)
    return searches
  end

  def search_for_user(search_string, original_id = nil)
    results_page = perform_search(search_string)
    result = get_result(results_page)
    unless result.nil?
      people_cache[result["uid"]] = result
      result["email"] = original_id unless original_id.nil?
      result["manager"] = get_managers(result["uid"])
      info_msg("Adding new user: #{result["full_name"]}, managers: #{result["manager"].size}")
      data_store << result
    end
    return result
  end

  def get_result(results_page)
    res_json_str = results_page.xpath("//script[ contains(text(), 'window.initialSearch')]").text.split("\n")[1]
    res_json = begin
      JSON.parse(/\{.*\}/.match(res_json_str)[0])
    rescue Exception => e
      error_msg("Parser error: #{e.message}")
      {"results" => []}
    end

    return res_json["results"].first
  end

  def get_managers(uid)
    managers = []
    profile_page = client.get(url + "/profiles/#{uid}")
    manager_link = profile_page.css("li[class='manager_link'] a").first
    unless manager_link.nil?
      manager_id = manager_link.attributes["href"].value.split("/").last
      manager = (people_cache[manager_id] || search_for_user(manager_id))
      managers << manager
      managers.concat(manager["manager"] || [])
    end
    return managers
  end

  def perform_search(un)
    search_form = search_page.forms.first
    search_form.field_with(name: "query").value = un
    search_form.submit
  end

	def login
    logon_page = client.get(url + "/login")
    form = logon_page.forms.first
    form.field_with(name: "session[uid]").value = username
    form.field_with(name: "session[password]").value = password
    form.submit
	end

	def client
    @client ||= Mechanize.new{|agent| agent.verify_mode = OpenSSL::SSL::VERIFY_NONE}
	end

  def people_cache
    @people_cache ||= {}
  end

  def search_page
    @search_page ||= login
  end

  class PeopleSearchExplorerJob
    include Sidekiq::Worker
    sidekiq_options :backtrace => true

    def perform(explorer_id, usernames)
      explorer = Explorer.find(explorer_id)
      explorer.search_for(usernames)
    end
  end
end