class Mapping < ActiveRecord::Base
  # relations
  has_many :mapping_keys, dependent: :destroy
  # we can be a mapping for a single explorer
  belongs_to :explorer
  # or the blueprint for all of them
  belongs_to :explorer_description

  # attributes
  attr_accessible :name, :description, :default_key, :opt_in

  def is_valid?
    keys_valid = !mapping_keys.empty? && mapping_keys.all?{|mk| mk.is_valid?}
    pk_mappings = mapping_keys.where("key LIKE '%?'", default_key)
    # check if default_key is not ignored if opt_in is used
  end

  # transform into a hash. This avoids DB lookups and simplifies the implementation of datastores
  def hashify
    mapping_hash = {:default_key => self.default_key}
    mapping_keys.each{|mk| mapping_hash.merge!(mk.hashify)}
    return mapping_hash
  end

  def alternatives
    # if we are a "general" mapping that does not belong anywhere, no alternatives
    explorer.nil? ? [] : explorer_description.mappings - [self]
  end

  def sample_with_html
    json = sample_json
    indexed_keys = [nil] + follow_hash("", json) + [nil]
    pretty_lines = []
    JSON.pretty_generate(json).lines.each_with_index do |line, i|
      if indexed_keys[i].nil?
        pretty_lines << strip_html(line)
      else
        pretty_lines << "<span data-full-key=\"#{indexed_keys[i]}\">#{strip_html(line)}</span>"
      end
    end
    return pretty_lines.join("")
  end

  def strip_html(str)
    ActionView::Base.full_sanitizer.sanitize(str)
  end

  def sample_json
    JSON.parse(File.open(explorer_description.sample_file).read)
  end

  def follow_hash(key_so_far, json)
    keys = []
    json.each_pair do |k, v|
      next_key = key_so_far.blank? ? k : "#{key_so_far}.#{k}"
      keys << next_key
      if v.is_a?(Array) && v.first.is_a?(Hash)
        keys << nil
        keys.concat(follow_hash(next_key, v.first))
        keys.concat([nil,nil])
      elsif v.is_a?(Array)
        (v.size + 1).times{keys << nil}
      elsif v.is_a?(Hash)
        keys.concat(follow_hash(next_key, v))
        keys << nil
      end
    end
    return keys
  end

  def keys_for(mk)
    direct_mappings = mapping_keys.where(key: mk)
    regexp_mappings = mapping_keys.where(is_regexp: true).select{|mapping_entry|
      regexp = Regexp.new(mapping_entry.key)
      mk.match(regexp)
    }
    return direct_mappings + regexp_mappings
  end

  def default_options
    sample_json.keys
  end

  def possible_move_targets(key)
    get_object_keys("", sample_json) - [key]
  end

  def get_object_keys(key_so_far, json)
    keys = []
    keys << key_so_far if json.is_a?(Hash)
    json.each_pair do |k,v|
      next_key = key_so_far.blank? ? k : "#{key_so_far}.#{k}"
      next_obj = v.is_a?(Array) ? v.first : v
      keys.concat(get_object_keys(next_key, next_obj)) if next_obj.is_a?(Hash)
    end
    return keys
  end

  def possible_primary_keys(key)
    json = sample_json
    key.split(".").each{|subkey| json = json[subkey]}
    json = json.first if json.is_a?(Array)
    if json.nil?
      return []
    else
      return json.keys
    end
  end

  def update_preview_graph
    ds = DataStore.new(self)
    x = Time.now
    ds.load_sample
    g = GraphViz.new("#{id}", {:type => :digraph, :splines => :true, rankdir: "LR"})
    node_cache = {}
    node_types = ds.nodes.collect{|n| n.type}.uniq
    palette = Paleta::Palette.generate(:type => :random, :size => node_types.size)

    ds.nodes.each_with_index do |node, i|
      node_cache[node] = g.add_node(i.to_s, {
        label: node.label_string,
        shape: :record,
        color: "##{palette[node_types.index(node.type)].hex}",
        fontsize: 35,
        penwidth: 4,
        style: :rounded
      })
    end
    ds.nodes.each do |node|
      node.all_relations.each do |rel|
        g.add_edge(node_cache[rel.source], node_cache[rel.target], {
          label: rel.name, labelfloat: false, fontsize: 30, labeldistance: 30.0, penwidth: 3
          })
      end
    end
    g.output(:png => preview_file_path)
    return true
  end

  def delete_preview_graph
    File.delete(preview_file_path) if File.exist?(preview_file_path)
  end

  def preview_file_path
    "public/mapping_previews/#{id}.png"
  end

  def preview_image_path
    "/mapping_previews/#{id}.png"
  end
end