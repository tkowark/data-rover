FactoryGirl.define do
  factory :user do
    email "user@example.com"
    password "12312356"
    password_confirmation { "12312356" }
  end
end