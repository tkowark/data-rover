class CreateExplorerInfos < ActiveRecord::Migration
  def change
    create_table :explorer_infos do |t|
      t.text :message
      t.string :event_type
      t.references :explorer
      t.boolean :data_import, default: false
      t.timestamps
    end

    add_index :explorer_infos, :explorer_id
  end
end
