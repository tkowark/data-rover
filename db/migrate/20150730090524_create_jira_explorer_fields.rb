class CreateJiraExplorerFields < ActiveRecord::Migration
  def change
    add_column :explorers, :projects, :string
    add_column :explorers, :context_path, :string
  end
end
