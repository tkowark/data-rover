# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20160926095549) do

  create_table "consumer_tokens", :force => true do |t|
    t.integer  "user_id"
    t.string   "provider",   :limit => 30
    t.string   "token",      :limit => 1024
    t.string   "secret"
    t.datetime "created_at",                 :null => false
    t.datetime "updated_at",                 :null => false
  end

  add_index "consumer_tokens", ["token"], :name => "index_consumer_tokens_on_token", :unique => true
  add_index "consumer_tokens", ["user_id"], :name => "index_consumer_tokens_on_user_id"

  create_table "explorer_descriptions", :force => true do |t|
    t.string   "name"
    t.text     "description"
    t.string   "version"
    t.string   "explorer_class_name"
    t.datetime "last_sample_update"
    t.boolean  "uses_oauth",          :default => false
    t.datetime "created_at",                             :null => false
    t.datetime "updated_at",                             :null => false
  end

  create_table "explorer_infos", :force => true do |t|
    t.text     "message"
    t.string   "event_type"
    t.integer  "explorer_id"
    t.boolean  "data_import", :default => false
    t.datetime "created_at",                     :null => false
    t.datetime "updated_at",                     :null => false
  end

  add_index "explorer_infos", ["explorer_id"], :name => "index_explorer_infos_on_explorer_id"

  create_table "explorers", :force => true do |t|
    t.integer  "explorer_description_id"
    t.integer  "mission_id"
    t.datetime "last_run"
    t.boolean  "update_samples",          :default => false
    t.datetime "start_date"
    t.datetime "end_date"
    t.string   "username"
    t.string   "password"
    t.string   "type"
    t.datetime "created_at",                                 :null => false
    t.datetime "updated_at",                                 :null => false
    t.string   "repo_name"
    t.string   "repo_owner"
    t.string   "client_id"
    t.string   "client_secret"
    t.string   "api_endpoint"
    t.string   "keywords"
    t.string   "filter"
    t.string   "repository_path"
    t.boolean  "get_patches",             :default => true
    t.string   "branches"
    t.string   "projects"
    t.string   "context_path"
    t.boolean  "parse_details",           :default => true
    t.string   "search_class"
    t.string   "search_attribute"
    t.string   "url"
    t.string   "csv_file_name"
    t.string   "csv_content_type"
    t.integer  "csv_file_size"
    t.datetime "csv_updated_at"
    t.string   "alias_hosts"
    t.boolean  "ignore_existing",         :default => true
    t.string   "components"
    t.boolean  "ignore_last_run_date",    :default => false
    t.string   "primary_key"
  end

  add_index "explorers", ["explorer_description_id"], :name => "index_explorers_on_explorer_description_id"
  add_index "explorers", ["mission_id"], :name => "index_explorers_on_mission_id"

  create_table "mapping_keys", :force => true do |t|
    t.string   "key"
    t.string   "datatype"
    t.text     "primary_key"
    t.string   "type_name"
    t.string   "move_to"
    t.string   "rename_to"
    t.string   "target_type"
    t.string   "foreign_key"
    t.boolean  "is_regexp"
    t.boolean  "ignore"
    t.boolean  "no_updates"
    t.boolean  "no_inserts"
    t.boolean  "contains_links"
    t.integer  "mapping_id"
    t.datetime "created_at",     :null => false
    t.datetime "updated_at",     :null => false
    t.boolean  "no_relation"
  end

  add_index "mapping_keys", ["mapping_id"], :name => "index_mapping_keys_on_mapping_id"

  create_table "mappings", :force => true do |t|
    t.string   "name"
    t.boolean  "opt_in",                  :default => false
    t.text     "description"
    t.text     "default_key"
    t.integer  "explorer_id"
    t.integer  "explorer_description_id"
    t.datetime "created_at",                                 :null => false
    t.datetime "updated_at",                                 :null => false
  end

  add_index "mappings", ["explorer_description_id"], :name => "index_mappings_on_explorer_description_id"
  add_index "mappings", ["explorer_id"], :name => "index_mappings_on_explorer_id"

  create_table "missions", :force => true do |t|
    t.datetime "last_run"
    t.integer  "user_id"
    t.integer  "interval"
    t.string   "name"
    t.text     "description"
    t.string   "database_name"
    t.string   "target_server"
    t.string   "username"
    t.string   "password"
    t.integer  "target_data_store_cd"
    t.boolean  "active",               :default => false
    t.datetime "created_at",                              :null => false
    t.datetime "updated_at",                              :null => false
  end

  add_index "missions", ["user_id"], :name => "index_missions_on_user_id"

  create_table "users", :force => true do |t|
    t.string   "email",                  :default => "", :null => false
    t.string   "encrypted_password",     :default => "", :null => false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          :default => 0
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at",                             :null => false
    t.datetime "updated_at",                             :null => false
  end

  add_index "users", ["email"], :name => "index_users_on_email", :unique => true
  add_index "users", ["reset_password_token"], :name => "index_users_on_reset_password_token", :unique => true

end
