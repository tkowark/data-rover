class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :token_authenticatable, :confirmable,
  # :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable, password_length: 5..128

  # Setup accessible (or protected) attributes for your model
  attr_accessible :email, :password, :password_confirmation, :remember_me, :provider, :uid, :name

  has_many :missions, dependent: :destroy

  # oauth tokens
  has_one :github_token, :class_name => "ConsumerToken", :conditions => proc {"provider = 'github'"}
  has_one :google_token, :class_name => "ConsumerToken", :conditions => proc {"provider = 'google_oauth2'"}
  has_one :twitter_token, :class_name => "ConsumerToken", :conditions => proc {"provider = 'twitter'"}
  has_one :trello_token, :class_name => "ConsumerToken", :conditions => proc {"provider = 'trello'"}
  has_one :facebook_token, :class_name => "ConsumerToken", :conditions => proc {"provider = 'facebook'"}

  has_many :consumer_tokens, :dependent => :destroy
end
