class CreateExplorerStackoverflows < ActiveRecord::Migration
  def change
    add_column :explorers, :keywords, :string
    add_column :explorers, :filter, :string
  end
end
