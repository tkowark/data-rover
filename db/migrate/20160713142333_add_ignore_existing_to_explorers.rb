class AddIgnoreExistingToExplorers < ActiveRecord::Migration
  def change
    add_column :explorers, :ignore_existing, :boolean, default: true
  end
end
