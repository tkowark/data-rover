source 'https://rubygems.org'

gem 'rails', '3.2.22'
gem 'yaml_db'
gem 'pg'
gem 'sqlite3'

# Gems used only for assets and not required
# in production environments by default.
group :assets do
  gem 'sass-rails',   '~> 3.2.3'
  gem 'coffee-rails', '~> 3.2.1'
  gem 'uglifier', '>= 1.0.3'
end

group :development, :test do
  gem 'factory_girl_rails'
  gem "rspec"
  gem "rspec-rails"
  gem 'simplecov'
end

gem 'jquery-rails'
gem 'jquery-ui-rails'
gem 'twitter-bootstrap-rails', '~> 2.2'
gem 'therubyracer'
gem "thin"
gem 'deep_cloneable'

# just so the production console runs ^^
gem 'minitest'
gem 'test-unit'

# the little helpers here and there...
gem 'devise'
gem "seedbank"
gem 'simple_enum'
gem "validate_url"
gem "ruby-prof"
gem 'simple_form'
gem 'datetimepicker-rails', git: 'https://github.com/zpaulovics/datetimepicker-rails.git', tag: 'v1.0.0'
gem "live_ast", :require => false
gem "font-awesome-rails"
gem "ruby-graphviz"
gem "paleta"
gem 'oj'

# explorer specific libraries
gem "rest-client"
gem 'omniauth'
gem "mechanize"
gem "octokit"
gem "sawyer"
gem 'tweetstream'
gem 'ruby-trello'
gem 'google-api-client'
gem 'jira-ruby', :require => 'jira'
gem "travis", :require => false
gem "paperclip"
gem "sanitize"
gem "highscore"
gem "capybara"
gem "poltergeist"
gem "public_suffix"
gem "geocoder"

# signin strategies
gem "omniauth-google-oauth2"
gem "omniauth-facebook"
gem "omniauth-github"
gem "omniauth-twitter"
gem "omniauth-trello"

# mongodb connection
gem 'mongo'
gem 'bson_ext'

# neo4j
gem 'neography'

# hana
gem 'ruby-odbc'

# rdbms
gem 'bitset'

# git reader
gem 'rugged'
gem 'iconv'

# peace of mind
gem 'rails_logtruncate'

# background working
gem 'sidekiq'
gem 'sinatra', :require => nil
gem 'whenever'
gem 'rubyzip'

# error moniotoring
gem 'airbrake', '~> 4.1'

# deployment
group :development do
  gem "capistrano", "~> 3.4"
  gem 'capistrano-rvm'
  gem 'capistrano-bundler'
  gem 'capistrano-rails'
  gem 'capistrano-rails-collection'
  gem 'capistrano-thin'
end
