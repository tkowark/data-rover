require 'rails_helper'

RSpec.describe RdbmsDataStore, :type => :model do

  it "should detect that two nodes require the same upsert statement" do
    n1 = Node.new("Thing", "key")
    n1.add_attribute("key", "unique_key")

    n2 = Node.new("Thing", "key")
    n2.add_attribute("key", "unique_key_as_well")

    ds = create_data_store([n1,n2])
    ds.upsert_nodes

    expect(ds.query_cache.size).to eq(1)
    expect(ds.query_cache["Thing"].size).to eq(1)
  end

  it "should detect that two nodes require different upsert statements" do
    n1 = Node.new("Thing", "key")
    n1.add_attribute("key", "unique_key")
    n1.add_attribute("a1", "v1")

    n2 = Node.new("Thing", "key")
    n2.add_attribute("key", "unique_key")
    n2.add_attribute("a2", "v1")

    n3 = Node.new("Another Thing", "key")
    n2.add_attribute("key", "unique_key")
    n2.add_attribute("a2", "v1")

    ds = create_data_store([n1,n2, n3])
    ds.upsert_nodes

    expect(ds.query_cache.size).to eq(2)

    expect(ds.query_cache["Thing"].size).to eq(2)
    expect(ds.query_cache["Thing"]["11"]).to_not be_nil
    expect(ds.query_cache["Thing"]["101"]).to_not be_nil

    expect(ds.query_cache["Another Thing"].size).to eq(1)
    expect(ds.query_cache["Thing"]["11"]).to_not be_nil
  end

  it "should add the primary key value as a final parameter" do
    n1 = Node.new("Thing", "key")
    n1.add_attribute("key", "unique_key")
    n1.add_attribute("a1", "v1")

    ds = create_data_store([n1])
    query, params = ds.prepare_upsert(n1)
    expect(query).to eq("UPSERT \"MYDB\".\"THINGS\" (\"KEY\", \"A1\") VALUES (?,?) WHERE \"KEY\" = ?")
    expect(params.size).to eq(3)
    expect(params.last).to eq(n1.primary_key_value)
  end

  def create_data_store(nodes = [])
    mapping = FactoryGirl.create(:mapping)
    allow(mapping).to receive(:hashify){{}}
    ds = RdbmsDataStore.new(mapping)
    allow(ds).to receive(:execute){}
    ds.nodes = nodes
    return ds
  end

end
