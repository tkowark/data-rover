class Explorer < ActiveRecord::Base
  belongs_to :explorer_description
  belongs_to :mission
  has_many :explorer_infos
  has_one :mapping, dependent: :destroy

  validates :explorer_description, :presence => true
  validates :mission, :presence => true
  attr_accessor :data_stores
  attr_accessible :update_samples

  after_create :build_mapping

  def self.model_name
    ActiveModel::Name.new(Explorer)
  end

  def build_mapping
    self.mapping = Mapping.create()
    self.explorer_description.mappings << self.mapping
    self.mapping.mapping_keys.create(key: "", type_name: "")
  end

  def replace_mapping(replacement)
    self.mapping.destroy
    self.mapping = replacement.deep_clone(include: :mapping_keys)
    self.mapping.save!
    return self.mapping
  end

  def explore!
    info_msg("Started parsing: " + editable_attributes.collect{|ea| "#{ea}: #{self.send(ea.to_sym)}"}.join(", "))
    begin
      prepare_database if self.last_run.nil?
      started = DateTime.now
      go_grabbing!
      self.last_run = started
      self.save!
      success_msg("Done parsing and uploading! Total time #{'%.2f' % (Time.now - started)}s")
    rescue Exception => e
      error_msg("Parsing finished with error: #{e.message}")
      raise e
    end
  end

  def enqueue!
    if Sidekiq::Queue.new(mission.explorer_queue).find{|job| job.args.first == self.id}.nil?
      Sidekiq::Client.push('queue' => mission.explorer_queue, 'class' => ExplorerJob, 'args' => [self.id])
      info_msg("enqueued for work!")
      return true
    else
      return false
    end
  end

  def go_grabbing!
    raise "implement me in my subclass!"
  end

  def editable_attributes()
    return self.class.accessible_attributes.select{|at| !at.blank?}
  end

  def identifying_name
    return self.explorer_description.name + " #{self.id}"
  end

  def task_queue()
    mission.explorer_task_queue
  end

  # returns a data store for a given mapping key
  def data_store()
    @data_store ||= DataStore.for_explorer(self)
  end

  def prepare_database
    data_store.initialize_db
  end

  def cleanup_database
    data_store.remove_duplicates
  end

  # logging...
  def error_msg(text)
    self.explorer_infos.create!(:event_type => ExplorerInfo::ERROR, :message => text)
  end

  def info_msg(text)
    self.explorer_infos.create!(:event_type => ExplorerInfo::INFO, :message => text)
  end

  def success_msg(text)
    self.explorer_infos.create!(:event_type => ExplorerInfo::SUCCESS, :message => text)
  end
end
