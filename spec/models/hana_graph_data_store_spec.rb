require 'rails_helper'

RSpec.describe HanaGraphDataStore, :type => :model do

  it "should add the primary key value as a final parameter" do
    n1 = Node.new("Thing", "key")
    n1.add_attribute("key", "unique_key")
    n1.add_attribute("a1", "v1")

    ds = create_data_store([n1])
    query, params = ds.prepare_upsert(n1)
    tf = ds.escaped(ds.type_field)
    uf = ds.escaped(ds.uri_field)
    expect(query).to eq("UPSERT #{ds.vertex_table} (\"KEY\", \"A1\", #{tf}, #{uf}) VALUES (?,?,?,?) WHERE #{uf} = ?")
    expect(params.size).to eq(5)
    expect(params[4]).to eq(n1.primary_key_value)
    expect(params[3]).to eq(n1.primary_key_value)
    expect(params[2]).to eq(n1.type)
  end

  def create_data_store(nodes = [])
    mapping = FactoryGirl.create(:mapping)
    allow(mapping).to receive(:hashify){{}}
    ds = HanaGraphDataStore.new(mapping)
    allow(ds).to receive(:execute){}
    allow(ds).to receive(:prepare){|x| x}
    ds.nodes = nodes
    return ds
  end

end
