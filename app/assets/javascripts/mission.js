$(document).ready(function() {
  $(".explorer-info-div").each(function(i, div){
    setInterval(function(){
        $.get(
          $(div).data()["callbackUrl"],
          {},
          function(data){
            $(div).html(data);
          },
          'html'
        );
    }, 5000);
  });
});