class GithubIssueEventsExplorer < GithubIssueDetailsExplorer

  def details_for_issue(issue_number, opts)
    client.issue_events(repo, issue_number, opts)
  end
end