class MongoDbDataStore < DataStore

  attr_accessor :collection, :database

  def database()
    @mongo_client ||= Mongo::MongoClient.new(explorer.mission.target_server)
    @database ||= @mongo_client.db(explorer.mission.database_name)
  end

  def collection
    if @collection.nil
      @collection = database[expl.collection_name]
      @collection.ensure_index(Hash[ *explorer.mapping[:_default].collect { |v| [ v, 1 ] }.flatten ])
    end
    return @collection
  end

  # thanks to MongoDBs upsert mechanism, we do it all in one go
  def upsert_nodes
    nodes.each{|node| upsert_node(node)}
  end

  def upsert_node()
    begin
      collection.update(
        {node.primary_key => node.primary_key_value},
        node.clean_attributes,
        {:upsert => true}
      )
    rescue Exception => e
      raise e
    end
  end

  # TODO: implement or ditch Mongo support
  def add_relationships()

  end

  def find_by_type_and_attributes(type, attribute_hash, return_values)
    return collection.find(attribute_hash, :fields => return_values)
  end
end