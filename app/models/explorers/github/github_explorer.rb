class GithubExplorer < Explorer
  attr_accessible :repo_name, :repo_owner, :api_endpoint, :client_secret
  attr_accessible :client_id, :start_date, :end_date, :ignore_last_run_date

  validate :repo_name, :presence => true
  validate :repo_owner, :presence => true

  def client
    if @client.nil?
      @client = Octokit::Client.new(
        :client_secret => self.client_secret.blank? ? OAUTH_CREDENTIALS["github"]["secret"] : self.client_secret,
        :client_id => self.client_id.blank? ? OAUTH_CREDENTIALS["github"]["key"] : self.client_id
      )
      @client.access_token = self.mission.user.github_token.token unless self.mission.user.github_token.nil?
      @client.api_endpoint = self.api_endpoint unless self.api_endpoint.blank?
      @client.proxy = ENV[@client.api_endpoint.start_with?("https://") ? "https_proxy" : "http_proxy"]
    end
    return @client
  end

  def repo
    {:username => self.repo_owner, :repo => self.repo_name}
  end

  def last_run_date
    self.last_run.iso8601
  end

  def perform_request(query_options = {}, &block)
    yield(query_options)
  rescue Octokit::TooManyRequests => e
    error_msg("#{e.class.name}: #{e.message}. pausing for #{client.rate_limit.resets_in} seconds before continuing")
    sleep(client.rate_limit.resets_in)
    perform_request(query_options, &block)
  rescue Faraday::ConnectionFailed => e
    error_msg("Connection problem. #{e.message}. Retrying in 60 seconds.")
    sleep(60)
    perform_request(query_options, &block)
  end

  def last_results
    client.last_response.data.collect{|d| d.to_attrs}
  end

  def each_page(query_options = {}, &block)
    perform_request(query_options, &block)
    while client.last_response.rels[:next]
      query_options[:page] = CGI.parse(URI.parse(client.last_response.rels[:next].href).query)["page"].first
      perform_request(query_options, &block)
    end
  end

  def paginate(query_options = {}, store_results = true, &block)
    results = []
    each_page(query_options) do |opts|
      yield(opts)
      if store_results
        res = last_results
        data_store.concat(res)
        results += res
        info_msg("Results so far: #{results.size}, page: #{opts[:page] || 1}")
      end
    end
    return results
  rescue Octokit::InternalServerError => e
    error_msg("Incomplete Parsing: #{e.message}")
    return results
  rescue Octokit::UnprocessableEntity => e
    error_msg("Request could not be answered: #{e.message}")
    return results
  end

  def std_opts(more_optns = {})
    opts = {}
    lower_limit = (ignore_last_run_date ? nil : last_run) || start_date
    opts[:since] = lower_limit.iso8601 unless lower_limit.nil?
    return opts.merge(more_optns)
  end
end