require 'rails_helper'

RSpec.describe Neo4jDataStore, :type => :model do

  it "should transform datetime attributes to timestamps" do
    mapping = FactoryGirl.create(:mapping)
    allow(mapping).to receive(:hashify){{"" => {primary_key: "url", type_name: "Thing"}, /_at$/ => {datatype: "dateTime"}}}
    n4ds = Neo4jDataStore.new(mapping)
    dateTime = DateTime.now
    n4ds.transform_object({"created_at" => dateTime, "updated_at" => dateTime, "at_with_too_much" => dateTime})
    expect(n4ds.nodes.size).to eq(1)
    expect(n4ds.nodes.first.get_attributes["updated_at_year"]).to eq(dateTime.year)
    expect(n4ds.nodes.first.get_attributes["at_with_too_much"]).to_not be_nil
    expect(n4ds.nodes.first.get_attributes["at_with_too_much"]).to eq(dateTime)
  end

end