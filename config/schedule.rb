# Use this file to easily define all of your cron jobs.
#
# It's helpful, but not entirely necessary to understand cron before proceeding.
# http://en.wikipedia.org/wiki/Cron

# Example:
#
set :output, "log/cron_log.log"
#
every 5.minutes do
   rake "missions:launch_if_ready"
   rake "cleanup:explorer_infos"
end

every 1.day do
   rake "data_import:gravitate"
end

every 2.day do
  rake "log:clear"
end

#
# every 4.days do
#   runner "AnotherModel.prune_old_records"
# end

# Learn more: http://github.com/javan/whenever
