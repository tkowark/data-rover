# Github Git Explorer
gh_git_explorer_descr = ExplorerDescription.find_by_explorer_class_name_and_version("GithubUserExplorer", "v1.0")
raise "Could not find description for Github User Explorer - run rake db:seeds:explorers first" if gh_git_explorer_descr.nil?
mapping = gh_git_explorer_descr.standard_mapping() || gh_git_explorer_descr.mappings.create()
mapping.update_attributes(:default_key => "url", name: "User to Node", description: "Creates a simple node for the user")
mapping.mapping_keys.destroy_all
mapping.mapping_keys.where(key: "").first_or_create.update_attributes(type_name: "GithubUser")