module ExplorersHelper

  def field_for_attribute(attribute, t_class, form)
    fn = ("field_for_" + attribute_type(t_class, attribute)).to_sym
    if self.respond_to?(fn)
      return self.send(fn, attribute, form)
    else
      return form.input(attribute)
    end
  end

  def attribute_type(t_class, attrib)
    if t_class.columns_hash[attrib].nil?
      t_class.columns_hash[attrib + "_file_name"].nil? ? "" : "file"
    else
      t_class.columns_hash[attrib].type.to_s
    end
  end

  def field_for_file(attribute, form)
    return form.input(attribute, as: :file)
  end

  def field_for_datetime(attribute, form)
    return form.input attribute, :as => :datetime_picker
  end

  def attribute_list(explorer)
    explorer.editable_attributes.collect do |attrib|
      val = explorer.send(attrib).to_s
      (val.blank? || attrib.include?("password")) ? nil : "<u>#{attrib}</u>: #{val}"
    end.compact.join(", ")
  end

end
