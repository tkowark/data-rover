require 'capybara/poltergeist'
require 'mechanize'

class WebsiteExplorer < Explorer

  # been there, done that -> URL cache so we don't end up in endless loops
  attr_accessor :btdt, :aliases
  attr_accessible :homepage, :alias_hosts

  def go_grabbing!
    parse_page(homepage_uri)
    info_msg("Visited #{btdt.size} pages. Parsing finished")
  end

  def parse_page(uri)
    return nil unless continue?(uri)

    page_json = {url: uri.to_s}
    page, status = get_page(uri)
    page_json[:status] = status
    return page_json if page.nil?

    visited!(uri)
    links = extract_links(page)
    headlines = find_headlines(page)
    people = extract_people(page)
    info_msg("Parsed #{uri}, found #{links.size} links and #{people.size} entities")

    data_store << page_json.merge({
      title: page.title,
      people: people,
      links: links.compact.collect{|link_uri| clean_uri(link_uri)}.uniq,
      keywords: keywords(page)
    }.merge(find_headlines(page)))

    links.each{|link_url| parse_page(link_url)}
  end

  def keywords(page)
    content = clean_string(Sanitize.fragment(page.content, {:remove_contents => ['script']}))
    highscore = Highscore::Content.new(content)
    highscore.keywords.top(5).collect{|kw| {text: kw.text}}
  end

  def clean_string(str)
    str.gsub("\n"," ").gsub("\t"," ")
  end

  def find_headlines(page)
    headlines = {}
    [1,2,3,4].each do |i|
      headlines["h#{i}"] = page.css("h#{i}").collect{|headline| {text: clean_string(headline.text)}}
    end
    return headlines
  end

  def get_page(uri)
    begin
      return client.get(uri), 200
    rescue Mechanize::UnauthorizedError => e
      return nil, 401
    rescue Mechanize::ResponseCodeError => e
      return nil, e.response_code
    end
  end

  def extract_links(page)
    page.links.reject{|link| link.uri.to_s.start_with?("javascript")}.collect{|link| full_uri(link.uri)}.compact
  end

  def continue?(uri)
    !visited?(uri) && in_domain?(uri)
  end

  def visited?(uri)
    !btdt[clean_uri(uri)].nil?
  end

  def visited!(uri)
    btdt[clean_uri(uri)] = true
  end

  def clean_uri(uri)
    uri.to_s.end_with?("/") ? uri.to_s[0..-2] : uri.to_s
  end

  def in_domain?(uri)
    domain(homepage_uri) == domain(uri) || aliases.any?{|ali| uri.host.end_with?(ali)}
  end

  def domain(uri)
    PublicSuffix.parse(uri.host).sld
  end

  def extract_people(page)
    # create a capybara session to handle the javascript
    session = Capybara::Session.new(:poltergeist_errorless)
    session.visit(page.uri)

    # select all links that are encrypted mail stuff
    people = page.links.select{|link| link.uri.to_s.start_with?("javascript:linkTo_UnCryptMailto")}.collect do |email_link|
      enc_string = email_link.uri.to_s.scan(/\('(.*?)'\)/).flatten.first
      mailto = ""
      offset = 0
      while(!mailto.include?("@") && offset > -5)
        mailto = get_email_address(session.evaluate_script("decryptString('#{enc_string}',#{offset -= 1});"))
      end
      mailto.blank? ? nil : {name: mailto.split("@").first, email: mailto}
    end

    session.driver.quit
    return people.compact
  end

  def get_email_address(decrypted)
    if decrypted.start_with?("mailto:")
      return decrypted.to_s.split("mailto:").last
    else
      return ""
    end
  end

  def full_uri(uri)
    return nil if uri.nil?
    uri.relative? ? (homepage_uri + uri) : uri
  end

  def homepage_uri()
    URI.parse(homepage)
  end

  def aliases
    @aliases ||= alias_hosts.split(",").collect{|host| host.strip}
  end

  def client
    @client ||= Mechanize::new
  end

  def btdt
    @btdt ||= {}
  end
end