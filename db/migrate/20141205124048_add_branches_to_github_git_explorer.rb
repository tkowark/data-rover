class AddBranchesToGithubGitExplorer < ActiveRecord::Migration
  def change
    add_column :explorers, :branches, :string
  end
end
