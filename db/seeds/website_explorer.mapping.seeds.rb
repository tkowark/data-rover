# Stackoverflow Explorer
ws_explorer_descr = ExplorerDescription.find_by_explorer_class_name_and_version("WebsiteExplorer", "v1.0")
raise "Could not find description for Website Explorer - run rake db:seeds:explorers first" if ws_explorer_descr.nil?

mapping = ws_explorer_descr.standard_mapping() || ws_explorer_descr.mappings.create()
mapping.update_attributes!(:default_key => "url", name: "Linked Mapping", description: "Creates a simple graph structure")
mapping.mapping_keys.destroy_all
mapping.mapping_keys.where(key: "").first_or_create.update_attributes(type_name: "Website")
mapping.mapping_keys.where(key: "people").first_or_create.update_attributes(type_name: "Entity", primary_key: "email")
mapping.mapping_keys.where(key: "links").first_or_create.update_attributes(type_name: "Website", foreign_key: "url", no_updates: true)
mapping.mapping_keys.where(key: "keywords").first_or_create.update_attributes(type_name: "Keyword", primary_key: "text")
mapping.mapping_keys.where(key: "h\\d").first_or_create.update_attributes(type_name: "Keyword", primary_key: "text", is_regexp: true)