module ExplorerInfosHelper
  
  def image_for_event(event_type)
    filename = case event_type
      when ExplorerInfo::ERROR then "cancel.png"
      when ExplorerInfo::SUCCESS then "accept.png"
      when ExplorerInfo::INFO then "information.png"  
      else "exclamation.png"
    end
    return image_tag("icons/#{filename}")
  end
  
end