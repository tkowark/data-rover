class AddNoRelationFlagToMapping < ActiveRecord::Migration
  def change
    add_column :mapping_keys, :no_relation, :boolean
  end
end
