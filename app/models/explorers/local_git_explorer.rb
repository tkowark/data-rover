class LocalGitExplorer < Explorer
  attr_accessible :repository_path, :start_date, :end_date, :get_patches, :parse_details, :ignore_existing
  attr_accessor :repo, :string_converter

  def go_grabbing!
    commits_shas = get_commit_info()
    get_commit_details(commits_shas) if parse_details
  end

  def repo
    @repo ||= Rugged::Repository.new(repository_path)
  end

  def get_commit_info
    shas = []
    ignore_us = ignore_existing? ? existing_commits() : {}

    commits = []
    info_msg("Parsing basic information. Ignoring #{ignore_us.size} commits already in the database.")
    repo.walk(repo.last_commit) do |commit|
      next if ignore_us.has_key?(commit.oid) || commit_out_of_range?(commit)
      commits << basic_commit_hash(commit)
      shas << commit.oid
      if commits.size.modulo(500) == 0
        data_store.concat(commits)
        info_msg("parsed #{data_store.size} commits")
        commits = []
      end
    end

    data_store.concat(commits)
    info_msg("Parsed basic information for #{data_store.size} commits")
    return shas
  end

  # TODO: make this depending on the current mapping for commits
  def existing_commits
    ignore_us = {}
    info_msg("Obtaining existing commits from the database...")
    begin
      data_store.find_by_type_and_attributes("Commit", {}, ["sha"]).each{|res| ignore_us[res["sha"]] = 0}
    rescue Exception => e
      error_msg("query error (parsing continues though ...): #{e.message}")
    end
    return ignore_us
  end

  def commit_out_of_range?(commit)
    out_of_range = false
    out_of_range ||= commit.author[:time] < start_date unless start_date.nil?
    out_of_range ||= commit.author[:time] > end_date unless end_date.nil?
    return out_of_range
  end

  def get_commit_details(shas = [])
    to_parse = ignore_existing ? open_commits : existing_commits.keys
    to_parse += shas
    to_parse.uniq!

    info_msg("Enqueing #{to_parse.size / 10} workers for #{to_parse.size} commits")
    slice_count = 0

    to_parse.each_slice(10) do |commit_shas|
      Sidekiq::Client.push('queue' => mission.explorer_task_queue, 'class' => LocalGitExplorerJob, 'args' => [self.id, commit_shas])
      info_msg("Enqued #{slice_count += 1} explorer tasks (#{slice_count * 10} commits)")
    end
  end

  def open_commits
    return data_store.find_by_type_and_attributes("Commit", {"total" => nil}, ["sha"]).collect{|res| res["sha"]}
  rescue
    []
  end

  def get_details(commits)
    start_time = Time.now
    merges = 0

    details = commits.collect do |sha|
      commit = repo.lookup(sha)
      file_changes = get_file_changes(commit, commit.parents.first)
      merges += 1 if commit.parents.size > 1
      stats = {
        "additions" => file_changes.inject(0){|sum,change| sum += change["additions"] || 0},
        "deletions" => file_changes.inject(0){|sum,change| sum += change["deletions"] || 0},
        "total" => file_changes.inject(0){|sum,change| sum += change["changes"] || 0}
      }
      detail_commit_hash(commit, stats, file_changes)
    end

    data_store.concat(details.compact)
    total_time = (Time.now - start_time)
    info_msg("Total #{total_time.round(2)}s, AVG #{(total_time / commits.size).round(1)}s, #{commits.size} commits, #{merges} merges")
  end

  def get_file_changes(commit, parent)
    return [] if parent.nil?

    diff = parent.diff(commit)
    deltas = diff.deltas

    if commit.parents.size != 1
      return deltas.collect{|delta| basic_file_change(commit, delta)}
    else
      return diff.collect.with_index{|patch, i| complete_file_change(commit, patch, deltas[i])}
    end
  end

  def complete_file_change(commit, patch, delta)
    fc = basic_file_change(commit, delta)
    fc.merge!({
      "changes" => patch.changes,
      "additions" => patch.additions,
      "deletions" => patch.deletions
    })

    if get_patches && !delta.binary?
      fc["patch"] = patch.to_s.gsub("\t"," ").gsub("\n"," ").gsub("\r"," ").squish[0..4999]
    end

    return fc
  end

  def basic_file_change(commit, delta)
    fc = {
      "filename" => delta.new_file[:path],
      "file_sha" => delta.old_file[:oid] + ":" + delta.new_file[:oid],
      "status" => delta.status.to_s
    }

    if delta.old_file[:path] != delta.new_file[:path]
      fc["old_filename"] = delta.old_file[:path]
    end

    return fc
  end

  def string_converter
    @string_converter ||= Iconv.new('UTF-8//IGNORE', 'UTF-8')
  end

  def save_string(str)
    begin
      string_converter.iconv(str)
    rescue Exception => e
      puts str + " --- has been removed"
      puts e.message
      "<this message has been removed>"
    end
  end

  def basic_commit_hash(commit, add_parents = true)
    c_hash = {
      "sha" => commit.oid,
      "merge_commit" => commit.parents.size == 2,
      "message" => save_string(commit.message)[0..4999],
      "committer" => user_hash(commit.committer),
      "author" => user_hash(commit.author),
      "committed_at" => commit.committer[:time],
      "authored_at" => commit.author[:time]
    }
    c_hash["parents"] = commit.parents.collect{|parent| basic_commit_hash(parent, false)} if add_parents
    return c_hash
  end

  def detail_commit_hash(commit, stats, files)
    {
      "sha" => commit.oid,
      "files" => files
    }.merge(stats)
  end

  def user_hash(user)
    return {
      "name" => save_string(user[:name]),
      "email" => save_string(user[:email]),
    }
  end

  class LocalGitExplorerJob
    include Sidekiq::Worker
    sidekiq_options :retry => true, :backtrace => true

    def perform(explorer_id, commit_shas)
      explorer = Explorer.find(explorer_id)
      explorer.get_details(commit_shas)
      explorer.repo.close
    end
  end
end