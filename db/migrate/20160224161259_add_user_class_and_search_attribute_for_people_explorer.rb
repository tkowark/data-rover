class AddUserClassAndSearchAttributeForPeopleExplorer < ActiveRecord::Migration
  def change
  	add_column :explorers, :user_class, :string
  	add_column :explorers, :search_attribute, :string
  	add_column :explorers, :url, :string
  	add_attachment :explorers, :csv
  end
end
