class JiraExplorer < Explorer
  attr_accessible :username, :password, :url, :context_path, :projects
  attr_accessor :client

  def go_grabbing!
    get_projects.each do |project|
      parse_issues(client.Project.find(project))
    end
  end

  def parse_issues(jira_project)
    start = 0

    loop do
      cur_issues = jira_project.issues({:expand => "changelog", :maxResults => 50, :startAt => start}).collect{|res| res.attrs}
      cur_issues.each_with_index do |issue, i|
        issue["comments"] = client.Issue.find(issue["key"]).comments.collect{|comment| comment.attrs}
      end

      data_store.concat(cur_issues)
      info_msg("request #{(start / 50) + 1}, results so far: #{data_store.size}")
      break if cur_issues.size != 50
      start += 50
    end
  end

  def client
    @client ||= JIRA::Client.new({
      :username => self.username,
      :password => self.password,
      :site     => self.url,
      :context_path => self.context_path,
      :auth_type => :basic,
      :ssl_verify_mode => OpenSSL::SSL::VERIFY_NONE
    })
  end

  def get_projects
    self.projects.split(",").collect{|pname| pname.strip}
  end
end