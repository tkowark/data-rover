class AddHomepageColumnForWebsiteExplorer < ActiveRecord::Migration
  def change
    add_column :explorers, :alias_hosts, :string
  end
end
