class ExplorerInfosController < ApplicationController

  def explorer
    @explorer = Explorer.find(params[:explorer_id])
    limit = params[:limit] || 100
    @infos = @explorer.explorer_infos.where(data_import: false).order("created_at DESC").limit(limit)
    @title = "Events for #{@explorer.identifying_name}"
    render "last_events", :layout => false  and return if limit == 5
  end

  def data_import
    limit = params[:limit] || 100
    exp_ids = Mission.find(params[:mission_id]).explorers.pluck(:id)
    @infos = ExplorerInfo.where(data_import: true, explorer_id: exp_ids).order("created_at DESC").limit(limit)
    render "last_events", :layout => false and return if limit == 5
  end
end