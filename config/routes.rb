DataKraken::Application.routes.draw do

  get '/auth/:provider/callback', to: 'oauth_consumers#callback'
  get '/auth/:provider/callback2', to: 'oauth_consumers#callback'

  resources :oauth_consumers, only: [:index]

  resources :missions do
    get "run_now"
    post "activate"
    post "deactivate"
    get "graph_file"
    get "prepare_database"
    get "cleanup_database"
    post "sidekiq_control"
    get "data_import_events", {controller: "explorer_infos", action: "data_import"}
    get "last_data_import_events", {controller: "explorer_infos", action: "data_import", limit: 5}
  end

  resources :explorers do
    get "events", {controller: "explorer_infos", action: "explorer"}
    get "last_events", {controller: "explorer_infos", action: "explorer", limit: 5}
    get "run_now"
  end

  resources :mappings do
    get :preview
    get :mk
  end

  resources :mapping_keys, only: [:update, :destroy] do
    post "add_field"
    post "remove_field"
  end

  devise_for :users
  require 'sidekiq/web'
  mount Sidekiq::Web => '/sidekiq'
  root to: "missions#index"
end
