class MappingKey < ActiveRecord::Base

  DATATYPES = ["dateTime", "text", "string", "longText", "integer"]

  # relationships
  belongs_to :mapping

  # unique identifier
  attr_accessible :key

  # attribute stuff
  attr_accessible :datatype, :primary_key, :type_name

  # altering
  attr_accessible :move_to, :rename_to

  # linking
  attr_accessible :foreign_key

  # boolean flags
  attr_accessible :ignore, :is_regexp, :no_updates, :no_inserts, :contains_links, :no_relation

  # only one mapping per key!
  validates :key, uniqueness: {scope: :mapping_id, message: "only one mapping per key unless you use regexps"}

  # TODO: check for weird combinations...
  def is_valid?
    true
  end

  def hashify
    {(is_regexp ? Regexp.new(key) : key) => hash_style_mapping}
  end

  def hash_style_mapping
    mp = {}
    available_attributes.collect do |attrib|
      val = self.send(attrib.to_sym)
      mp[attrib.to_sym] = val unless val.nil?
    end
    return mp
  end

  def unset_attributes
    available_attributes.select{|attr| self.send(attr.to_sym).nil?}
  end

  def available_attributes
    MappingKey.accessible_attributes.select{|a| !a.blank?} - ["key", "is_regexp"]
  end

  def possible_primary_keys
    mapping.possible_primary_keys(key)
  end

  def corresponding_primary_key
    primary_key || mapping.default_key
  end

  def possible_move_targets
    mapping.possible_move_targets(key)
  end

  def display_key
    if is_regexp
      return "/#{key}/"
    else
      return "\"#{key}\""
    end
  end
end
