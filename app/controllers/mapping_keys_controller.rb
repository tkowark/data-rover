class MappingKeysController < ApplicationController

  layout :false

  def update
    mapping_entry = MappingKey.find(params[:id])
    if params[:mapping_key][:key].start_with?("/")
      params[:mapping_key][:key] = params[:mapping_key][:key][1..-2]
      params[:mapping_key][:is_regexp] = true
    else
      params[:mapping_key][:key].gsub!("\"", "")
    end
    mapping_entry.update_attributes(params[:mapping_key])
    render partial: "mappings/mapping_key", locals: {mk: mapping_entry}
  end

  def destroy
    mk = MappingKey.find(params[:id])
    unless mk.key == ""
      mk.destroy
      render text: ""
    else
      render partial: "mappings/mapping_key", locals: {mk: mk}
    end
  end

  def add_field
    mk = MappingKey.find(params[:mapping_key_id])
    new_val = MappingKey.columns_hash[params[:new_field]].type == :boolean ? true : ""
    new_val = MappingKey::DATATYPES.first if params[:new_field] == "datatype"
    mk.update_attributes(params[:new_field] => new_val)
    render partial: "mappings/mapping_key", locals: {mk: mk}
  end

  def remove_field
    mk = MappingKey.find(params[:mapping_key_id])
    mk.update_attributes(params[:field] => nil)
    render partial: "mappings/mapping_key", locals: {mk: mk}
  end
end