ExplorerDescription.where(name:"Stackoverflow Question Explorer", version: "v2.1").first_or_create!(
  uses_oauth: true,
  description: "Gets Stackoverflow questions for a given keyword.",
  explorer_class_name: "StackoverflowExplorer"
)

ExplorerDescription.where(name: "Github Commit Explorer", version: "v3.0").first_or_create!(
  uses_oauth: true,
  description: "Extracts Git commit information for a given github repository and owner. Will smartly use the datetime of the last run to avoid costly stalls.",
  explorer_class_name: "GithubGitExplorer"
)

ExplorerDescription.where(name: "Github Issue Explorer", version: "v3.0").first_or_create!(
  uses_oauth: true,
  description: "Extracts Issue tracking information for a given github repository and owner.",
  explorer_class_name: "GithubIssuesExplorer"
)

ExplorerDescription.where(name: "Github Issue Events Explorer", version: "v3.0").first_or_create!(
  uses_oauth: true,
  description: "Extracts Issue tracking information for a given github repository and owner.",
  explorer_class_name: "GithubIssueEventsExplorer"
)

ExplorerDescription.where(name: "Github Issue Comments Explorer", version: "v3.0").first_or_create!(
  uses_oauth: true,
  description: "Extracts Issue tracking information for a given github repository and owner.",
  explorer_class_name: "GithubIssueCommentsExplorer"
)

ExplorerDescription.where(name: "Google Drive Explorer", version: "v1.0").first_or_create!(
  uses_oauth: true,
  description: "Extracts metadata from Google Drive folders (who edited what when...).",
  explorer_class_name: "GoogleDriveExplorer"
)

ExplorerDescription.where(name: "Local Git Repository Explorer", version: "v1.0").first_or_create!(
  uses_oauth: false,
  description: "Extracts commit information from a git repository running locally.",
  explorer_class_name: "LocalGitExplorer"
)

ExplorerDescription.where(name: "Metric Fu Explorer", version: "v1.0").first_or_create!(
  uses_oauth: false,
  description: "Extracts metric_fu information from a git repository running locally.",
  explorer_class_name: "MetricFuExplorer"
)

ExplorerDescription.where(name: "Jira Explorer", version: "v1.0").first_or_create!(
  uses_oauth: false,
  description: "Parses issue data from Jira.",
  explorer_class_name: "JiraExplorer"
)

ExplorerDescription.where(name: "Travis-CI Explorer", version: "v1.0").first_or_create!(
  uses_oauth: true,
  description: "Collects build information from Travis-CI.",
  explorer_class_name: "TravisCiExplorer"
)

ExplorerDescription.where(name: "People Explorer", version: "v1.0").first_or_create!(
  uses_oauth: false,
  description: "Parses additional information from the internal people directory.",
  explorer_class_name: "PeopleExplorer"
)

ExplorerDescription.where(name: "Website Explorer", version: "v1.0").first_or_create!(
  uses_oauth: false,
  description: "Parses a website and all of its subpages by following the internal links",
  explorer_class_name: "WebsiteExplorer"
)

ExplorerDescription.where(name: "Bugzilla Explorer", version: "v1.0").first_or_create!(
  uses_oauth: false,
  description: "Extracts Issue tracking information from Bugzilla for a given or all components.",
  explorer_class_name: "BugzillaExplorer"
)

ExplorerDescription.where(name: "Github User Explorer", version: "v1.0").first_or_create!(
  uses_oauth: true,
  description: "Gets some additional information about Github Users.",
  explorer_class_name: "GithubUserExplorer"
)

ExplorerDescription.where(name: "Github Pull Request Explorer", version: "v1.0").first_or_create!(
  uses_oauth: true,
  description: "Gets some additional information about Github Users.",
  explorer_class_name: "GithubPullRequestExplorer"
)