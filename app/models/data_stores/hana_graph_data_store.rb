class HanaGraphDataStore < HanaDataStore

  def custom_node_fields()
    [type_field, uri_field]
  end

  def custom_node_params(node)
    [node.type, node.primary_key_value, node.primary_key_value]
  end

  def relation_fields(relation)
    [source_field, target_field, type_field]
  end

  def relation_values(relation)
    [relation.source_id, relation.target_id, relation.name]
  end

  def vertex_table
    schema + "." + "VERTICES"
  end

  def edge_table
    schema + "." + "EDGES"
  end

  def data_tables()
    vt = Table.new(vertex_table, [
      Column.new(escaped(uri_field), "VARCHAR(256)", true),
      Column.new(escaped(type_field), "VARCHAR(256)")
    ])

    extra_columns = {}
    nodes.each do |node|
      node.clean_attributes.each_pair do |k,v|
        extra_columns[k] ||= Column.new(escaped(k), column_type(k,v))
      end
    end
    vt.columns.concat(extra_columns.values)
    return [vt]
  end

  def join_tables()
    et = Table.new(edge_table, [
      auto_increment_id_field,
      Column.new(escaped(type_field), "VARCHAR(256)"),
      Column.new(escaped(source_field), "VARCHAR(256)"),
      Column.new(escaped(target_field), "VARCHAR(256)")
    ])
    return [et]
  end

  def pk_field(node)
    uri_field
  end

  def node_table(node)
    vertex_table
  end

  def relation_table(relation)
    edge_table
  end

  def uri_field
    "COM.HPI.URI"
  end

  def source_field
    "COM.HPI.SOURCE"
  end

  def target_field
    "COM.HPI.TARGET"
  end

  def type_field
    "COM.HPI.TYPE"
  end
end