class DeleteInternalKeyForMappings < ActiveRecord::Migration
  def change
    remove_column :mappings, :internal_key
  end
end
