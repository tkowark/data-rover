class StackoverflowExplorer < Explorer
  attr_accessible :keywords

  def go_grabbing!()
    has_more = true
    page = 0
    mech = Mechanize.new

    while(has_more)
      info_msg("Getting page #{page + 1}")
      cur_page = mech.get("https://api.stackexchange.com/2.1/search?" + query_options(page += 1).join("&"))
      page_json = JSON.parse(cur_page.body)
      data_store.concat(page_json["items"])
      unless page_json["backoff"].nil?
        info_msg("Sleeping for #{page_json["backoff"]}")
        sleep(page_json["backoff"])
      end
      info_msg("Parsed #{page} pages. Questions so far: #{data_store.size}")
      has_more = page_json["has_more"]
    end
  end

  def query_options(page)
    return {
      "order" => "desc",
      "sort" => "votes",
      "tagged" => self.keywords,
      "site" => "stackoverflow",
      "pagesize" => "100",
      "page" => page,
      "key" => "kgb7VXFoQvnDY4stkqo9DQ%28%28",
      "filter" => "%21BSqXpicXnvtZL%29Q7yNX6Lao9kER6tk"
    }.collect{|k,v| "#{k}=#{v}"}
  end

end
