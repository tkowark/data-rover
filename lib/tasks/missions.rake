namespace :missions do

  desc "checks all mission and starts the ones that are due to run"
  task :launch_if_ready => [:environment] do
    missions = Mission.where(:active => true)
    puts "[#{DateTime.now}] checking Mission status for #{missions.size} missions"
    missions.each do |mission|
      if mission.should_run_now?
        puts "Launching mission #{mission.name}"
        mission.launch_explorers!
      end
    end
  end
end
