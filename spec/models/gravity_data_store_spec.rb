require 'rails_helper'

RSpec.describe GravityDataStore, :type => :model do

  before(:each) do
    FileUtils.rm_rf(graph_file)
  end

  after(:each) do
    FileUtils.rm_rf(graph_file)
  end

  it "add relationships correctly when returned from neo4j" do
    mapping = FactoryGirl.create(:mapping)
    allow(mapping).to receive(:hashify){{}}
    gds = GravityDataStore.new(mapping)
  end

  def graph_file
    Rails.root.join("spec", "sample_outputs", "gg.jsdg").to_s
  end
end