namespace :mappings do

  desc "checks all mission and starts the ones that are due to run"
  task :refresh_samples, [:explorer_name, :explorer_id] => [:environment] do |t, args|
    each_mapping(args[:explorer_name], args[:explorer_id]) do |ds|
      puts "loading sample json from: #{ds.sample_input_file}"
      ds.load_sample
      puts "transformed sample, storing output to: #{ds.sample_output_file}"
      ds.write_sample_output
    end
  end

  desc "profiles the run of a sample file, which you should put into samples/json and name,e.g. local_git_explorer_<mk>_large.json"
  task :profile, [:explorer_name, :explorer_id] => [:environment] do |t, args|
    each_mapping(args[:explorer_name], args[:explorer_id]) do |ds|
      if !File.exist?(ds.benchmark_input_file)
        puts "benchmark file not found: #{ds.benchmark_input_file}"
        next
      end
      puts "starting benchmark with file #{ds.benchmark_input_file}"
      RubyProf.start
      ds.load_benchmark
      result = RubyProf.stop
      puts "Profiling finished. Created #{ds.nodes.size} nodes."
      File.open(Rails.root.join("samples","profiling","#{args[:explorer_name]}(#{ds.mapping}).html"),"w+") do |file|
        RubyProf::GraphHtmlPrinter.new(result).print(file)
      end
    end
  end

  def each_mapping(explorer_name, explorer_id)
    raise "Please call task with explorer name and/or id: ['GithubIssueExplorer', 11] or ['JiraExplorer']" if explorer_name.nil?
    explorer_description = ExplorerDescription.find_by_explorer_class_name(explorer_name)
    raise "Could not find description for explorer #{explorer_name}" if explorer_description.nil?

    explorer_description.mapping_keys.each do |mk|
      mapping = explorer_description.mappings.where(explorer_id: explorer_id).first
      ds = DataStore.new(mapping)
      yield(ds)
    end
  end
end