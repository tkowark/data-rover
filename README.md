# DataRover
Lightweight collection and storage of software repository data

![DataRover_ASE_Demo_2016.png](https://bitbucket.org/repo/x7M7XL/images/351544000-DataRover_ASE_Demo_2016.png)

## Setup using Vagrant
* Ensure **Vagrant** and a VM provider (probably **Virtualbox**) is installed.
Hashicorp recommends downloading Vagrant packages from their [website](https://www.vagrantup.com/downloads.html).
Virtualbox can be installed with your package manager, e.g. `sudo apt-get install virtualbox`

~~~~
$ vagrant --version
Vagrant 1.8.5

$ virtualbox --help
Oracle VM VirtualBox Manager 4.3.36_Ubuntu
...
~~~~

* Ensure the Vagrant plugins **berkshelf, proxyconf and share** are installed.

~~~~
$ vagrant plugin list
vagrant-berkshelf (5.0.0)
vagrant-proxyconf (1.5.2)
vagrant-share (1.1.5)
~~~~

Or install them with `vagrant plugin install <name>`

* Install **Berkshelf** (part of the Chef Development Kit) for managing Chef cookbooks.
It can be downloaded from their [website](https://downloads.chef.io/chef-dk/).
Follow the directions to [configure and run berks](http://berkshelf.com/), reproduced below:

~~~~
$ PATH=$HOME/.chefdk/gem/ruby/2.1.0/bin:/opt/chefdk/bin:$PATH
$ berks --version
4.3.5
$ cd data-rover/vm
$ berks install
Resolving cookbook dependencies...
~~~~

* Start up the vagrant VM (in the directory with the Vagrantfile):

~~~~
$ cd vm
$ vagrant up
~~~~

* In case of errors while provisioning, check the `proxy` settings in the Vagrantfile and make sure the VM has internet access. Internet access is required in order to install chef and dependencies.

~~~~
$ vagrant ssh
vagrant@vagrant-ubuntu-trusty-64:~$ wget https://www.rubygems.org
~~~~

* Configure the Vagrant VM by installing **gem dependencies and Neo4J**, setting up the database and starting the webserver:

~~~~
$ vagrant ssh
vagrant@vagrant-ubuntu-trusty-64:~$ cd data-rover
vagrant@vagrant-ubuntu-trusty-64:~/data-rover$ bundle --version
Bundler version 1.12.5
vagrant@vagrant-ubuntu-trusty-64:~/data-rover$ bundle install

vagrant@vagrant-ubuntu-trusty-64:~/data-rover$ sudo apt-get install openjdk-7-jre
vagrant@vagrant-ubuntu-trusty-64:~/data-rover$ rake neo4j:install
vagrant@vagrant-ubuntu-trusty-64:~/data-rover$ nano neo4j/conf/neo4j-server.properties
# Set dbms.security.auth_enabled=false
# Uncomment org.neo4j.server.webserver.address=0.0.0.0
vagrant@vagrant-ubuntu-trusty-64:~/data-rover$ rake neo4j:start

vagrant@vagrant-ubuntu-trusty-64:~/data-rover$ rake db:migrate && rake db:seed

vagrant@vagrant-ubuntu-trusty-64:~/data-rover$ rails s -p 4000
~~~~

* Configure OmniAuth for services you want DataRover to connect to
~~~~
- create /config/oauth.yml (you can use the oauth.yml.default as a template)
- For Github, register a developer application (Github.com -> Settings -> OAuth Applications -> Developer Applications -> Register a new application)
- Callback URL: http://localhost:<port>/auth/github/callback
- Enter app key and secret in oauth.yml
~~~~
* Connect to Service
~~~~
- Restart Application Server
- Login
- Go to "Connected APIs"
- Click on your target API
- Grant access for DataRover
~~~~