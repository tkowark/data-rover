class MappingsController < ApplicationController

  def preview
    @mapping = Mapping.find(params[:mapping_id])
    begin
      @mapping.delete_preview_graph
      @mapping.update_preview_graph
    rescue Exception => e
      @mapping_error = e.message
    end
    render layout: false
  end

  def update
    mapping = Mapping.find(params[:id])
    # load the template if the user selected this option
    if params[:load_template]
      mapping = mapping.explorer.replace_mapping(Mapping.find(params[:replacement_mapping_id]))
    else
      mapping.update_attributes(params[:mapping])
    end
    redirect_to mapping
  end

  def show
    @mapping = Mapping.find(params[:id])
    @explorer = @mapping.explorer
  end

  def mk
    mapping = Mapping.find(params[:mapping_id])
    highlighted = mapping.keys_for(params[:key])

    if highlighted.empty?
      new_key = mapping.mapping_keys.create(key: params[:key])
      render partial: "mapping_key", locals: {mk: new_key} and return
    else
      render json: highlighted.collect{|mk| "mapping-entry-#{mk.id}"}
    end
  end
end
