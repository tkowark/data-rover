class AddComponentsToExplorers < ActiveRecord::Migration
  def change
  	add_column :explorers, :components, :string
  end
end
