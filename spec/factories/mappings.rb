FactoryGirl.define do
  factory :mapping do
    default_key "url"
    opt_in false
    explorer{FactoryGirl.create(:explorer)}
  end

end
