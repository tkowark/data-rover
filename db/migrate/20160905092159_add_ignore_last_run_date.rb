class AddIgnoreLastRunDate < ActiveRecord::Migration
  def change
    add_column :explorers, :ignore_last_run_date, :boolean, default: false
  end
end
