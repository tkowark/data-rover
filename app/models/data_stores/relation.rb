class Relation < Struct.new(:name, :target, :source)

  attr_accessor :source_id, :target_id

  def equal_to(other_relation)
    return false if target.primary_key_value.blank? || other_relation.target.primary_key_value.blank?
    name == other_relation.name && target.primary_key_value == other_relation.target.primary_key_value
  end

  def nicely_formatted()
    "#{name}: #{source.type} (#{source.primary_key_value}) --> #{target.type} (#{target.primary_key_value})"
  end

  def source_id
    source.nil? ? @source_id : source.db_id
  end

  def target_id
    target.nil? ? @target_id : target.db_id
  end
end