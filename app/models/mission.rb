class Mission < ActiveRecord::Base
  attr_accessible :name, :description, :target_server, :interval, :database_name, :target_data_store, :username, :password
  as_enum :target_data_store, %i{mongo_db neo4j hana_graph gravity hana}

  belongs_to :user
  has_many :explorers, :dependent => :destroy
  validates :user, :presence => true
  validates :target_server, :presence => true

  before_destroy :teardown_sidekiqs

  ALWAYS = 0
  HOURLY = 60
  TWICE_A_DAY = 720
  DAILY = 1440

  SIDEKIQS = ["data_import", "explorers", "explorer_tasks"]

  def should_run_now?
    if self.interval == ALWAYS || last_run.nil?
      return true
    else
      return (Time.now - last_run) > (self.interval * 60)
    end
  end

  def launch_explorers!
    explorers.each{|explorer| explorer.enqueue!}
    self.last_run = DateTime.now
    self.save
  end

  def unique_name
    "#{name}_#{id}"
  end

  def prepare_database
    explorers.each{|explorer| explorer.prepare_database}
  end

  def cleanup_database
    explorers.each{|explorer| explorer.cleanup_database}
  end

  def start_sidekiq(which_one)
    Dir.chdir(Rails.root.to_s) do
      target_path = sidekiq_config(which_one)
      adapt_template_config(default_config(which_one), target_path) unless File.exist?(target_path)
      system("bundle exec sidekiq -C #{sidekiq_config(which_one).to_s} -d")
    end
  end

  def teardown_sidekiqs
    SIDEKIQS.each do |sidekiq|
      FileUtils.rm_rf(sidekiq_config(sidekiq))
      stop_sidekiq(sidekiq)
      Sidekiq::Queue.new(sidekiq_queue(sidekiq)).clear
    end
    return true
  end

  def stop_sidekiq(which_one)
    Dir.chdir(Rails.root.to_s) do
      system("bundle exec sidekiqctl stop tmp/pids/sidekiq_#{which_one}_#{id}.pid")
    end
    return true
  end

  def adapt_template_config(template_file, target_path)
    template = File.open(template_file).read
    template.gsub!("<% mission_id %>", "#{id}")
    File.open(target_path,"w+"){|f| f.puts template}
  end

  def default_config(which_one)
    "config/sidekiq/#{which_one}.yml.default"
  end

  def sidekiq_config(which_one)
    "config/sidekiq/#{which_one}_#{id}.yml"
  end

  def sidekiq_queue(which_one)
    "mission_#{id}_#{which_one}"
  end

  def explorer_queue()
    sidekiq_queue("explorers")
  end

  def data_import_queue()
    sidekiq_queue("data_import")
  end

  def explorer_task_queue
    sidekiq_queue("explorer_tasks")
  end
end