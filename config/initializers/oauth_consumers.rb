unless defined? OAUTH_CREDENTIALS
  OAUTH_CREDENTIALS = if File.exists?(Rails.root.join("config/oauth.yml"))
    YAML.load(File.open(Rails.root.join("config/oauth.yml")).read)
  else
    {}
  end
end

Rails.application.config.middleware.use OmniAuth::Builder do
  OAUTH_CREDENTIALS.each_pair do |oauth_provider, options|
    provider(oauth_provider, options["key"], options["secret"], options)
  end
end