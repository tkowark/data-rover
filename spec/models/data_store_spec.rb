require 'rails_helper'

RSpec.describe DataStore, :type => :model do

  it "should create the proper class for the given mission" do
    explorer = FactoryGirl.create(:explorer)
    Mission.target_data_stores.each{|tds|
      explorer.mission.target_data_store = tds[0]
      r = DataStore.for_explorer(explorer)
      assert_equal Object.const_get("#{tds[0].to_s.classify}DataStore"), r.class
    }
  end

  it "should throw an error if we don't use a specialised result set" do
    r = DataStore.new(FactoryGirl.create(:mapping))
    [:initialize_db, :upsert_nodes].each do |i_method|
      expect{r.send(i_method)}.to raise_error(/implement/)
    end
    expect{r.find_by_type_and_attributes("Type", {}, [])}.to raise_error(/implement/)
  end

  it "should concat something, increase the size variable and call store" do
    r = DataStore.new(FactoryGirl.create(:mapping))
    expect(r).to receive(:store).once
    r.concat([1,2,3,4])
    assert_equal 4, r.size
  end

  it "should concat something, increase the size variable and call store" do
    r = DataStore.new(FactoryGirl.build(:mapping))
    expect(r).to receive(:store).once
    r << 1
    assert_equal 1, r.size
  end

  it "should properly merge hashes for sample extraction" do
    r = DataStore.new(FactoryGirl.build(:mapping))
    joined = r.update_sample({"a" => [{"c" => "d"}]}, [{"a" => [{"e" => "f"}]}])
    assert joined["a"].first.has_key?("c")
    assert joined["a"].first.has_key?("e")
  end

  it "should not overwrite existing values through nil and empty stuff" do
    r = DataStore.new(FactoryGirl.build(:mapping))
    joined = r.update_sample({"a" => {"c" => "d"}}, [{"a" => nil}])
    assert_not_nil joined["a"]
    assert joined["a"].has_key?("c")
  end

  it "should retain existing values" do
    r = DataStore.new(FactoryGirl.build(:mapping))
    joined = r.update_sample({"a" => "hello", "b" => {"c" => "world"}}, [{"a" => "goodbye", "b" => {"c" => "dude"}}])
    assert_equal "hello", joined["a"]
    assert_equal "world", joined["b"]["c"]
  end

  it "should not remove existing values in hashes" do
    r = DataStore.new(FactoryGirl.build(:mapping))
    joined = r.update_sample({"a" => {"c" => {"d" => "e", "f" => "g"}}}, [{"a" => {"b" => "g"}}])
    assert_not_nil joined["a"]["c"]
    assert joined["a"]["c"].has_key?("d")
    assert joined["a"]["c"].has_key?("f")
  end


  it "should find regexp if no direct mapping is available" do
    mapping = FactoryGirl.create(:mapping)
    allow(mapping).to receive(:hashify){{/world/ => {:type_name => "Type"}, "hello.world" => {:type_name => "different type"}}}
    r = DataStore.new(mapping)
    assert_equal "different type", r.get_mapping("hello.world")[:type_name]
    assert_equal "Type", r.get_mapping("notinthere.hello.world")[:type_name]
  end

  it "should handle the third case..." do
    map_hash = {"" => {:type_name => "Thing"}, "value" => {:datatype => "something"}, "something" => {:move_to => ""}}
    mapping = FactoryGirl.create(:mapping)
    allow(mapping).to receive(:hashify){map_hash}
    r = DataStore.new(mapping)
    expect(r.get_mapping("thing.value.subthing.value")[:datatype]).to be_nil
    expect(r.get_mapping("thing.value.nothing.value")[:datatype]).to be_nil
    expect(r.get_mapping("id")[:fake]).to eq(true)
  end

  it "should ditch upsert only nodes after lookup" do
    map_hash = {
      "" => {type_name: "Thing"},
      "subthing" => {type_name: "SubThing"},
      "subthing.link" => {foreign_key: "login", type_name: "LinkThing", no_inserts: true},
      :default_key => "url"
    }
    mapping = FactoryGirl.create(:mapping)
    allow(mapping).to receive(:hashify){map_hash}
    ds = DataStore.new(mapping)
    ds.transform_object({"url" => "http://example.org", "subthing" => {"link" => "Dude", "url" => "http://example2.org"}})
    # the three nodes are "", "subthing", and the Thing linked by "subthing.link"
    expect(ds.nodes.size).to eq(3)
    # we should, however, only insert the first two if the last one has no database_id
    link_thing = ds.nodes.find{|n| n.type == "LinkThing"}
    expect(link_thing.insert).to eq(false)
    expect(ds.nodes_to_insert).to_not include(link_thing)
    expect(ds.nodes_to_update).to be_empty
    link_thing.db_id = 1234
    expect(ds.nodes_to_update.size).to eq(1)
  end

  it "should create foreign key links only once" do
    mapping = create_mapping({
      "" => {type_name: "Question"},
      /comments$/ => {type_name: "Comment"},
      /owner$/ => {type_name: "User"},
      /answers$/ => {type_name: "Answer"},
      /display_name$/ => {foreign_key: "login", type_name: "GhUser", no_inserts: true, rename_to: "same_as"},
      :default_key => "link"
    })

    ds = DataStore.new(mapping)
    json = {
      "link" => "question1",
      "answers" => [{
        "link" => "answer1",
        "owner" => {"display_name" => "tkowark", "link" => "user1"}
      },{
        "link" => "answer2",
        "owner" => {"display_name" => "tgrabs", "link" => "user2"},
        "comments" => [{
          "link" => "comment1",
          "owner" => {"display_name" => "ollieg", "link" => "user3"}
        },{
          "link" => "comment2",
          "owner" => {"display_name" => "olliefant", "link" => "user4"}
        }]
      }],
      "owner" => {"display_name" => "bernddasbrot", "link" => "user5"}
    }
    ds.transform_object(json)
    expect(ds.nodes.select{|n| n.type == "User"}.size).to eq(5)
    expect(ds.nodes.select{|n| n.type == "GhUser"}.size).to eq(5)
    expect(ds.nodes.select{|n| n.type == "Answer"}.size).to eq(2)
    expect(ds.nodes.select{|n| n.type == "Comment"}.size).to eq(2)
    expect(ds.nodes.select{|n| n.type == "Question"}.size).to eq(1)

    (1..5).each{|i| expect(ds.nodes.select{|n| n.primary_key_value == "user#{i}"}.size).to eq(1)}
  end

  it "should not try to update stuff we said is insert only" do
    mapping = create_mapping({
      "" => {type_name: "Thing"},
      "subthing" => {type_name: "SubThing", no_updates: true},
      :default_key => "url"
    })

    ds = DataStore.new(mapping)
    ds.transform_object({"url" => "http://example.org", "subthing" => {"link" => "Dude", "url" => "http://example2.org"}})
    st = ds.nodes.find{|n| n.type == "SubThing"}
    expect(st).to_not be_nil
    expect(st.update).to eq(false)
    expect(st.insert).to eq(true)
    expect(ds.nodes_to_insert).to include(st)
    st.db_id = 1234
    expect(ds.nodes_to_update).to_not include(st)
  end

  it "should not follow stuff that is set to no updates" do
    mapping = create_mapping({
      "" => {type_name: "User"},
      /organisation/ => {move_to: "", type_name: "Organisation"},
      "manager" => {type_name: "User", no_updates: true},
      :default_key => "url"
    })

    ds = DataStore.new(mapping)
    ds.transform_object({"url" => "http://example.org/user",
      "organisation" => {"url" => "http://example.org/orga1"},
      "manager" => {
        "url" => "http://example.org/manager1",
        "organisation" => {"url" => "http://example.org/orga2"}
      }
    })

    st = ds.nodes.select{|n| n.type == "User"}
    expect(st.size).to eq(2)
    expect(ds.nodes.find{|n| n.type == "Organisation"}).to be_nil
  end

  it "should set the update property for nodes that appear multiple times" do
    mapping = create_mapping({
      default_key: "url",
      "" => {type_name: "Thing"},
      "subthing" => {type_name: "Thing", no_updates: true}
    })
    ds = DataStore.new(mapping)
    ds.transform_object({"url" => "http://example.org"})
    expect(ds.nodes.first.update).to eq(true)
    ds.transform_object({"url" => "http://example2.org", "subthing" => {"url" => "http://example.org"}})
    expect(ds.nodes.all?{|n| n.update}).to eq(true)
    ds.transform_object({"url" => "http://example3.org", "subthing" => {"url" => "http://example4.org"}})
    ex4 = ds.nodes.find{|n| n.primary_key_value == "http://example4.org"}
    expect(ex4.update).to eq(false)
    ds.transform_object({"url" => "http://example4.org"})
    expect(ex4.update).to eq(true)
  end

  it "should upsert nodes from multiple transformations" do
    mapping = create_mapping({default_key: "url", "" => {type_name: "Thing"}, "subthing" => {type_name: "Thing"}})
    ds = DataStore.new(mapping)
    ds.transform_object({"url" => "http://example.org", "attrib1" => false})
    ds.transform_object({"url" => "http://example.org", "attrib2" => "World"})
    expect(ds.nodes.size).to eq(1)
    expect(ds.nodes.first.get_attributes["attrib1"]).to eq(false)
    expect(ds.nodes.first.get_attributes["attrib2"]).to eq("World")
    ds.transform_object({"url" => "http://example2.org", "subthing" => {"url" => "http://example.org", "attrib1" => true}})
    expect(ds.nodes.size).to eq(2)
    expect(ds.nodes.first.get_attributes["attrib1"]).to eq(true)
  end

  it "should use arrays of stuff as foreign keys if we ask it to" do
    mapping = create_mapping({default_key: "url", "" => {type_name: "Thing"}, "subthing" => {type_name: "Thing", foreign_key: "url"}})
    ds = DataStore.new(mapping)
    ds.transform_object({"url" => "http://example.org", "subthing" => ["http://example.org"]})
    expect(ds.nodes.size).to eq(1)
    expect(ds.nodes.first.all_relations.size).to eq(1)
  end

  it "should use character classes in regexps" do
    mapping = create_mapping({/h\d/ => {type_name: "Something"}})
    ds = DataStore.new(mapping)
    expect(ds.map_hash[/h\d/]).to_not be_nil
    expect(ds.get_mapping("h2")[:type_name]).to eq("Something")
    expect(ds.get_mapping("hx")[:type_name]).to be_nil
  end

  it "should ignore foreign key links without a valid URL" do
    mapping = create_mapping({
      default_key: "url", "" => {type_name: "Thing"},
      "stuff_url" => {type_name: "Stuff", rename_to: "stuff", foreign_key: "url"}
    })

    ds = DataStore.new(mapping)
    ds.transform_object({"url" => "http://example.org/Thing1", "stuff_url" => nil})
    ds.transform_object({"url" => "http://example.org/Thing2", "stuff_url" => "http://example.org/Stuff1"})
    expect(ds.nodes_to_insert.size).to eq(3)
  end

  it "should make proper use of internal lookups" do
    mapping = create_mapping({default_key: "url", "" => {type_name: "Thing"}, "subthing" => {type_name: "Thing"}})
    objects = [
      {"url" => "http://example.org/thing1"},
      {"url" => "http://example.org/thing2", "subthing" => {"url" => "http://example.org/thing1"}},
      {"url" => "http://example.org/thing3", "subthing" => {"url" => "http://example.org/thing4"}},
      {"url" => "http://example.org/thing4"}
    ]
    ds = DataStore.new(mapping)
    objects.each{|obj| ds.transform_object(obj)}
    expect(ds.nodes_to_insert.size).to eq(4)
  end

  it "should work regardless of json or string keys" do
    mapping = create_mapping({
      default_key: "url",
      "" => {type_name: "Thing"},
      "subthing.key" => {move_to: ""}
    })
    mapping.opt_in = true
    ds = DataStore.new(mapping)
    ds.transform_object({
      :url => "https://example.org/thing1",
      :subthing => {:key => "going down"}
    })
    expect(ds.nodes.size).to eq(1)
    expect(ds.nodes.first.get_attributes["subthing.key"]).to_not be_nil
  end

  def create_mapping(map_hash)
    mapping = FactoryGirl.create(:mapping)
    allow(mapping).to receive(:hashify){map_hash}
    return mapping
  end
end