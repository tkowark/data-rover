class MissionsController < ApplicationController
  before_filter :authenticate_user!, :except => [:graph_file]

  def show
    @mission = Mission.find(params[:id])
    @descriptions = ExplorerDescription.all
    @title = "Mission Overview - '#{@mission.name}'"
    flash[:notice] = "No explorers configured. Please add some to your job!" if @mission.explorers.empty?
  end

  def index
    @missions = current_user.missions
    if @missions.blank?
      redirect_to new_mission_path(), {:notice => "No missions available, create one, young explorer!"}
    end
  end

  def destroy
    @mission = Mission.find(params[:id])
    if @mission.destroy
      redirect_to missions_path, {:notice => "Mission #{params[:id]} successfully deleted."}
    else
      redirect_to missions_path, {:alert => "Mission #{params[:id]} could not deleted."}
    end
  end

  def create
    @mission = current_user.missions.build(params[:mission])
    if @mission.save
      redirect_to @mission, :notice => 'Mission was successfully created.'
    else
      render :action =>  "new"
    end
  end

  def new
    @mission = Mission.new()
  end

  def run_now
    @mission = Mission.find(params[:mission_id])
    @mission.launch_explorers!
    redirect_to @mission, :notice => "Activated inactive explorers instantly!"
  end

  def activate
    @mission = Mission.find(params[:mission_id])
    @mission.active = true
    @mission.save
    redirect_to :action => :index
  end

  def deactivate
    @mission = Mission.find(params[:mission_id])
    @mission.active = false
    @mission.save
    redirect_to :action => :index
  end

  def update
    @mission = Mission.find(params[:id])
    if @mission.update_attributes(params[:mission])
      redirect_to @mission, :notice => 'Mission was successfully updated.'
    else
      redirect_to @mission, :alert => 'Mission could not be updated.'
    end
  end

  def edit
    @mission = Mission.find(params[:id])
  end

  def graph_file
    @mission = Mission.find(params[:mission_id])
    if @mission.target_data_store == :gravity
      send_file @mission.explorers.first.data_store.graph_file, type: Mime::JSON
    else
      redirect_to @mission and return
    end
  end

  def prepare_database
    @mission = Mission.find(params[:mission_id])
    begin
      @mission.prepare_database
      redirect_to @mission, :notice => 'Database was successfully prepared.'
    rescue Exception => e
      redirect_to @mission, :alert => "Error during database initialization: #{e.message}"
    end
  end

  def cleanup_database
    @mission = Mission.find(params[:mission_id])
    begin
      @mission.cleanup_database
      redirect_to @mission, :notice => 'Database was successfully cleansed.'
    rescue Exception => e
      redirect_to @mission, :alert => "Error during database initialization: #{e.message}"
    end
  end

  def sidekiq_control
    @mission = Mission.find(params[:mission_id])
    sidekiqs = params[:sidekiqs] == "all" ? Mission::SIDEKIQS : [params[:sidekiqs]]
    begin
      sidekiqs.each do |sk|
        @mission.start_sidekiq(sk) if params[:start]
        @mission.stop_sidekiq(sk) if params[:stop]
      end

      redirect_to @mission, :notice => "Sidekiqs (#{sidekiqs.join(', ')}) have been #{params[:start] || params[:stop]}ed."
    rescue Exception => e
      redirect_to @mission, :alert => "Error during sidekiq initialization: #{e.message}"
    end
  end
end