class OauthConsumersController < ApplicationController
  before_filter :authenticate_user!, :only=>:index

  def index
    @connected_services = ConsumerToken.all(:conditions => {:user_id => current_user.id}).collect{|ct| ct.provider}
    @available_services = OAUTH_CREDENTIALS.keys.collect{|oap| oap.to_s} - @connected_services
  end

  def callback
    @consumer_token = current_user.consumer_tokens.where(:provider => params[:provider]).first
    @consumer_token ||= current_user.consumer_tokens.create(:provider => params[:provider])
    @consumer_token.token = auth_hash[:credentials][:token]
    @consumer_token.secret = auth_hash[:credentials][:secret]
    @consumer_token.save
    redirect_to oauth_consumers_path, :notice => "Successfully connected to #{params[:provider]}"
  end

  protected

  def auth_hash
    request.env['omniauth.auth']
  end
end
