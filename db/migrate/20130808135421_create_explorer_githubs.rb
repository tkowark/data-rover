class CreateExplorerGithubs < ActiveRecord::Migration
  def change
    add_column :explorers, :repo_name, :string
    add_column :explorers, :repo_owner, :string
    add_column :explorers, :client_id, :string
    add_column :explorers, :client_secret, :string
    add_column :explorers, :api_endpoint, :string
  end
end