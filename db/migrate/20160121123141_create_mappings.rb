class CreateMappings < ActiveRecord::Migration
  def change
    create_table :mappings do |t|
      t.string :internal_key
      t.string :name
      t.boolean :opt_in, default: false
      t.text :description
      t.text :default_key
      t.references :explorer
      t.references :explorer_description
      t.timestamps
    end

    add_index :mappings, :explorer_id
    add_index :mappings, :explorer_description_id
  end
end
