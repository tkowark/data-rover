class AddPrimaryKeyToExplorers < ActiveRecord::Migration
  def change
    add_column :explorers, :primary_key, :string
  end
end
