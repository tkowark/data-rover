namespace :data do

  desc "refreshes the jsdg files for all gravity jobs"
  task :gravitate => [:environment] do
    Mission.where(target_data_store_cd: Mission.target_data_stores[:gravity]).each do |mission|
      puts "creating jsdg for mission #{mission.name}"
      mission.explorers.sample.data_store.create_jsdg
    end
  end
end