# config valid only for current version of Capistrano
lock '3.5.0'

set :application, 'data-rover'
set :repo_url, 'git@bitbucket.org:tkowark/data-rover.git'
set :thin_config_path, -> { "#{shared_path}/config/thin.yml" }
set :linked_files, %w{config/database.yml config/thin.yml config/oauth.yml}
set :whenever_roles, ->{ [:web, :app]}

# dirs we want symlinking to shared
set :linked_dirs, %w{bin log tmp/pids tmp/cache tmp/sockets vendor/bundle public/system tmpdata public/csv config/sidekiq public/mapping_previews samples/json}
set :format, :pretty
#set :log_level, :info

set(:config_files, %w(
  database.yml.default
  thin.yml.default
  log_rotation
  sidekiq/data_import.yml.default
  sidekiq/explorer_tasks.yml.default
  sidekiq/explorer.yml.default
))

set :rvm_ruby_version, '2.2.4'

namespace :sidekiq do
  desc "Restarts Sidekiq Workers"
  task :restart do
    on roles(:app) do
      within "#{current_path}" do
        with rails_env: :production do
          execute :rake, "setup:restart_sidekiqs"
        end
      end
    end
  end
end

namespace :deploy do
  after 'deploy:publishing', 'thin:restart'
  after 'deploy:publishing', 'sidekiq:restart'
end