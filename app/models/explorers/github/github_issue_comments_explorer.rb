class GithubIssueCommentsExplorer < GithubIssueDetailsExplorer

  def details_for_issue(issue_number, opts)
    client.issue_comments(repo, issue_number, opts)
  end
end