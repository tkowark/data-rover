class BugzillaExplorer < Explorer

	attr_accessible :url, :username, :password, :components, :parse_details

  def bug_fields
    ["priority", "bug_status", "bug_severity", "resolution"]
  end

	def go_grabbing!
    product_component_list.each_pair do |product, comps|
      comps.each do |component|
        next unless parse_component(component)
        params = {"product" => product, "component" => component}
        all_bugs = get_bugs_with_params(params, 0)
        data_store.concat(all_bugs)
        info_msg("Found #{all_bugs.size} bugs for #{product}:#{component}")
      end
    end
	end

  def parse_component(component)
    return true if components.blank?
    return components.split(",").map(&:strip).include?(component)
  end

  def get_bugs_with_params(params, next_field_index)
    bugs = Set.new(parse_bugs(get_bugs(params)))
    info_msg("Found #{bugs.size} bugs with search #{params}")
    if bugs.size == 500 && next_field_index < bug_fields.size
      next_field = bug_fields[next_field_index]
      possible_values(next_field).each do |value|
        bugs.merge(get_bugs_with_params(params.merge({next_field => value}), next_field_index + 1))
      end
    end
    return bugs
  end

  def get_bugs(params)
    search_form = main_page.forms.find{|form| form.name == 'queryform'}
    (bug_fields + params.keys).each do |field_name|
      search_form.field_with(name: field_name).value = params[field_name]
    end
    return search_form.submit
  end

  def parse_bugs(res_page)
    res_page.css("tr[class*='bz_bugitem']").collect do |bug|
      bug_info = {}
      bug.css("td").each do |el|
        el_name = el.attributes["class"].value.split(" ").last.scan(/bz_(.*?)_column/).flatten.first
        el_value = if el.css("span").first.nil? || el.css("span").first.attributes["title"].nil?
          if el.css("a").first.nil?
            el.inner_html
          else
            el.css("a").first.inner_html
          end
        else
          el.css("span").first.attributes["title"].to_s
        end
        bug_info[el_name] = el_value.strip.gsub("\n", "").gsub("\t","")
      end
      bug_info["url"] = self.url + "/show_bug.cgi?id=#{bug_info["id"]}"
      if parse_details
        info_msg("Getting detail for bug #{bug_info}")
        bug_info.merge!(get_bug_details(bug_info["url"])) 
      end
      bug_info
    end
  end

  def get_bug_details(url)
    page = client.get(url)
    details = {}
    page.css("td[class='field_label']").each do |td|
      next unless td.css("b").children.first.is_a?(Nokogiri::XML::Text)
      attrib = td.css("b").children.first.to_s.strip.downcase.split.join("_")
      value = td.parent.css("td").last.inner_html.strip
      details[attrib] = value
    end
    return details
  end

  def possible_values(field)
    main_page.css("select[name='#{field}'] option").collect{|el| el.attributes["value"].value}
  end

  def products
    main_page.css("select[name='product'] option").collect{|el|
      [el.attributes["id"].value.scan(/v(.*?)_product/).flatten.first, el.attributes["value"].value]
    }
  end

  def valid_component?(component)
    parse_components.include?(component)
  end

  def get_components
    components.blank? ? parse_components : components.split(",").map(&:strip)
  end

  def parse_components
    main_page.css("select[name='component'] option").collect{|el| 
      [el.attributes["id"].value.scan(/v(.*?)_component/).flatten.first, el.attributes["value"].value]
    }
  end

  def product_component_list
    list = {}
    assignments = main_page.body.scan(/showValueWhen\('component', (.*?), 'product',(.*?), true\)/)
    comps = parse_components
    products.each do |prod|
      valid_comps = JSON.parse(assignments.find{|ass| ass.last == prod.first}.first)
      list[prod.last] = comps.select{|comp| valid_comps.include?(comp.first.to_i)}.map(&:last)
    end
    return list
  end

  def main_page
    @main_page ||= login
  end

  def login
    page = client.get(url + "/query.cgi?format=advanced  ")
    form = page.forms.find{|f| f.fields.any?{|field| field.name == "Bugzilla_login"}}
    form.field_with(name: "Bugzilla_login").value = username
    form.field_with(name: "Bugzilla_password").value = password
    form.submit
  end

  def client
    @client ||= Mechanize.new{|agent| agent.verify_mode = OpenSSL::SSL::VERIFY_NONE}
  end

end