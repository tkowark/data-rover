class CreateExplorerDescriptions < ActiveRecord::Migration
  def change
    create_table :explorer_descriptions do |t|
      t.string :name
      t.text :description
      t.string :version
      t.string :explorer_class_name
      t.text :mapping_keys
      t.datetime :last_sample_update
      t.boolean :uses_oauth, :default => false
      t.timestamps
    end
  end
end
