namespace :demo do

  desc "run the demo with the given parameters"
  task :prepare, [:project_name, :email, :password] => [:environment] do |t, args|
    email = args[:email] || "admin@datarover.de"
    password = args[:password] || "admin"
    Rake::Task["setup:create_user"].invoke(email, password)
    user = User.where(email: email).first
    user.missions.where(name: "Collect #{args[:project_name]}").first_or_create(target_server: "http://localhost:7474", target_data_store: :neo4j)
    Rake::Task["neo4j:reset_yes_i_am_sure"].invoke
  end

  desc "geocodes the user locations of a HANA mission"
  task :geocode_users, [:project_id] => [:environment] do |t, args|
  	mission = Mission.find(args[:project_id])
  	explorer = mission.explorers.where(:type => "PeopleExplorer").first
  	ds = DataStore.for_explorer(explorer)
  	utype = ds.map_hash[""][:type_name]
  	locs = ds.find_by_type_and_attributes(utype, {}, ["location"])
  	locs.uniq.each do |loc_info|
  		next if loc_info["location"].nil?
  		results = Geocoder.search(loc_info["location"])
  		if results.empty?
  			puts "No results for #{loc_info["location"]}"
  		else
  			c = results.first.coordinates
  			puts "Setting coordinates #{c} for #{loc_info["location"]}"
  			sql = "UPDATE #{ds.node_table(utype)} SET LATITUDE = #{c[0]}, LONGITUDE = #{c[1]} WHERE LOCATION = '#{loc_info["location"]}'"
  			ds.run_query(sql)
  		end
  	end
  end
end

