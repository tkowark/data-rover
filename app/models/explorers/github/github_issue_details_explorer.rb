class GithubIssueDetailsExplorer < GithubExplorer

  attr_accessible :search_class, :search_attribute, :primary_key

  def go_grabbing!()
    issues = data_store.find_by_type_and_attributes(search_class, {}, [search_attribute, primary_key]).collect do |res|
      res[search_attribute].nil? ? nil : {"number" => res[search_attribute], "url" => res[primary_key]}
    end.compact
    issues.each_slice(20).each{|issue_infos| enqueue_job(issue_infos)}
    info_msg("Enqueued #{issues.size / 20} #{which_details} jobs for #{issues.size} issues")
  end

  def enqueue_job(issue_info)
    Sidekiq::Client.push(
      'queue' => mission.explorer_task_queue,
      'class' => GhDetailsJob,
      'args' => [self.id, issue_info]
    )
  end

  def get_details(issue_infos)
    issue_infos.each do |issue_info|
      paginate(std_opts, false) do |opts|
        details_for_issue(issue_info["number"], opts)
        details = last_results
        details.each{|lr| lr[:issue_url] = issue_info["url"]}
        data_store.concat(details)
      end
    end
    success_msg("Obtained #{data_store.size} #{which_details} for #{issue_infos.size} issues")
  end

  def details_for_issue(issue_number, opts)
    raise "implement me"
  end

  def which_details
    self.class.name.scan(/GithubIssue(.*?)Explorer/).flatten.first
  end

  class GhDetailsJob
    include Sidekiq::Worker
    sidekiq_options :retry => true, :backtrace => true

    def perform(explorer_id, issue_infos)
      Explorer.find(explorer_id).get_details(issue_infos)
    end
  end
end