# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :mission do
    name "Sample Mission"
    description "Sample Description"
    target_server "172.0.0.1:27027"
    interval 3600
    database_name "mydb"
    target_data_store 1
    user
  end
end
