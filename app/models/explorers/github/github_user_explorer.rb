class GithubUserExplorer < GithubExplorer

  attr_accessible :search_class, :search_attribute

  def go_grabbing!
    usernames = all_users.collect{|u| u[search_attribute]}
    info_msg("Getting details for #{usernames.size} users")
    usernames.each.with_index do |username, i|
      perform_request{|opts| data_store << client.user(username).to_attrs}
      info_msg("Got details for #{i+1}/#{usernames.size} users") if (i + 1).modulo(10) == 0
    end
  end

  def all_users
    if search_class.blank? || search_attribute.blank?
      raise "Specify search_class and search_attribute for parser"
    else
      data_store.find_by_type_and_attributes(search_class, {}, [search_attribute])
    end
  end
end