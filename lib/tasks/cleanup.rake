namespace :cleanup do

  desc "cleans up all the mess in the jobs table and the tmpdata folder"
  task :jobs => [:environment] do
    puts "== removing all open jobs"
    Mission.all.each do |mission|
      Mission::SIDEKIQS.each{|sk| Sidekiq::Queue.new(mission.sidekiq_queue(sk)).clear}
    end
    puts "== cleaning tmpdata folder"
    FileUtils.rm_rf(Dir.glob('tmpdata/*.json'), secure: true)
    FileUtils.rm_rf(Dir.glob('tmpdata/*.zip'), secure: true)
  end

  desc "removes explorer infos and only retains the last 100 for each explorer"
  task :explorer_infos => [:environment] do
    Explorer.all.each do |explorer|
      explorer.explorer_infos.where(data_import: true).order('id desc').offset(100).destroy_all
      explorer.explorer_infos.where(data_import: false).order('id desc').offset(100).destroy_all
    end
  end
end