class CreateExplorers < ActiveRecord::Migration
  def change
    create_table :explorers do |t|
      t.references :explorer_description
      t.references :mission
      t.datetime :last_run
      t.boolean :update_samples, default: false
      t.datetime :start_date
      t.datetime :end_date
      t.string :username
      t.string :password
      t.string :type
      t.timestamps
    end

    add_index :explorers, :explorer_description_id
    add_index :explorers, :mission_id
  end
end
