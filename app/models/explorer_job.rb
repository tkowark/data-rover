class ExplorerJob
  include Sidekiq::Worker
  sidekiq_options :queue => :explorers

  def perform(explorer_id)
    Explorer.find(explorer_id).explore!
  end
end