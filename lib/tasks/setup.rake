namespace :setup do

  desc "stops all sidekiq servers"
  task :stop_sidekiqs => [:environment] do
    Mission.all.each do |m|
      puts "Stopping Sidekiq Workers for mission #{m.id}"
      Mission::SIDEKIQS.each{|sk| m.stop_sidekiq(sk)}
    end
  end

  desc "starts the sidekiqs for all active missions"
  task :start_sidekiqs => [:environment] do
    Mission.where(active: true).each do |m|
      puts "Starting Sidekiq Workers for mission #{m.id}"
      Mission::SIDEKIQS.each{|sk| m.start_sidekiq(sk)}
    end
  end

  desc "performs stop and start of all sidekiqs"
  task :restart_sidekiqs => [:environment, "setup:stop_sidekiqs", "setup:start_sidekiqs"]
end