class GithubIssuesExplorer < GithubExplorer

  def go_grabbing!
    open_issues = paginate(std_opts({:state => :open})){|opts| client.list_issues(repo, opts)}
    success_msg("Fetched #{open_issues.size} open issues.")
    closed_issues = paginate(std_opts({:state => :closed})){|opts| client.list_issues(repo, opts)}
    success_msg("Fetched #{closed_issues.size} closed issues.")
  end
end