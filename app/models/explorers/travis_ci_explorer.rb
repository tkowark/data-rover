class TravisCiExplorer < Explorer

  attr_accessible :repo_name, :repo_owner

  def go_grabbing!
    builds = []
    repo.each_build do |build|
      attrs = build.attributes
      attrs["url"] = "https://travis-ci.org/#{repo_owner}/#{repo_name}/build/#{attrs["number"]}"
      attrs["commit"] = build.commit.attributes
      attrs["commit"]["url"] = "https://api.github.com/repos/#{repo_owner}/#{repo_name}/commits/#{attrs["commit"]["sha"]}"
      builds << attrs
      if (builds.size).modulo(100) == 0
        data_store.concat(builds)
        builds = []
        info_msg("Parsed #{data_store.size} builds")
      end
    end
    data_store.concat(builds)
  end

  def repo
    client.repo("#{repo_owner}/#{repo_name}")
  end

  # TODO: use the Github OAuth Token
  def client
    @client ||= Travis::Client.new
  end
end