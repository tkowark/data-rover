class RdbmsDataStore < DataStore

  attr_accessor :schema, :query_cache, :field_cache

  COLUMN_TYPES = {
    "Float" => "FLOAT",
    "Double" => "DOUBLE",
    "Fixnum" => "BIGINT",
    "Bignum" => "BIGINT",
    "ODBC::TimeStamp" => "TIMESTAMP",
    "ODBC::Time" => "TIME",
    "ODBC::Date" => "DATE",
    "DateTime" => "TIMESTAMP",
    "Time" => "TIME",
    "Date" => "DATE",
    "String" => "NVARCHAR(5000)",
    "FalseClass" => "BOOLEAN",
    "TrueClass" => "BOOLEAN"
  }

  def schema
    @schema ||= escaped(explorer.mission.database_name)
  end

  def upsert_nodes()
    return if nodes.empty?
    start = Time.now
    rel_count = 0
    node_stats = Hash.new(0)
    nodes.each do |node|
      node.db_id = node.primary_key_value
      nu_query, params = prepare_upsert(node)
      execute(nu_query, params)
      node_stats[node.type] += 1
      rel_count += insert_relations(node)
    end

    total_time = (Time.now - start).round(2)
    opsps = ((nodes.size + rel_count) / total_time).round
    info("#{opsps} ops/s, total: #{total_time}s, nodes: #{node_stats}, #{rel_count} relations")
  ensure
    drop_statements()
  end

  def query_cache
    @query_cache ||= {}
  end

  def field_cache(node)
    @field_cache ||= {}
    @field_cache[node.type] ||= {node.primary_key => 0}
  end

  def prepare_upsert(node)
    fc = field_cache(node)
    node_params = []

    node.get_attributes.each_pair do |key, value|
      fc[key] ||= fc.size
      node_params[fc[key]] = value.is_a?(Symbol) ? value.to_s : value
    end

    fingerprint = Bitset.new(node_params.size)
    node_params.each.with_index{|x, i| fingerprint[i] = true unless x.nil?}
    return get_query(node, fingerprint), node_params.compact + custom_node_params(node)
  end

  def insert_relations(node)
    query_cache[:__rels] ||= {}
    node.all_relations.each do |rel|
      query_cache[:__rels][rel.name] ||= prepare(insert_stmt(relation_table(rel), relation_fields(rel)))
      execute(query_cache[:__rels][rel.name], relation_values(rel))
    end.size
  end

  def custom_node_params(node)
    [node.primary_key_value]
  end

  def custom_node_fields()
    []
  end

  def get_query(node, fingerprint)
    query_cache[node.type] ||= {}
    query_cache[node.type][fingerprint.to_s] ||= prepare(create_stmt(node, fingerprint))
  end

  def create_stmt(node, fingerprint)
    set_fields = []
    node_fields = field_cache(node)
    fingerprint.each_with_index{|val, i| set_fields << node_fields.keys[i] if val}
    upsert_stmt(node_table(node.type), set_fields + custom_node_fields, pk_field(node))
  end

  def add_relationships()
    return true
  end

  def initialize_db
    load_sample
    create_schema
    create_tables
  end

  def create_schema
    run_stmt(create_schema_stmt(), /duplicate schema/)
  end

  def create_schema_stmt()
    "CREATE SCHEMA #{schema}"
  end

  # this will give you tables for each node type + join tables
  def all_tables
    return data_tables + join_tables.uniq
  end

  def data_tables
    nodes.group_by{|node| node.type}.collect do |type, type_nodes|
      columns = {}
      type_nodes.each do |node|
        node.clean_attributes.each_pair{|k,v| columns[escaped(k)] ||= Column.new(escaped(k), column_type(k,v))}
      end
      Table.new(node_table(type), columns.values)
    end
  end

  def join_tables
    all_relationships.collect do |relation|
      Table.new(relation_table(relation), relation_columns(relation))
    end.uniq
  end

  def node_table(node_type)
    schema + "." + escaped(node_type.pluralize)
  end

  def relation_table(relation)
    schema + "." + escaped(relation.source.type.pluralize + "_" + relation.target.type.pluralize + "_" + relation.name)
  end

  def relation_columns(relation)
    cols = [relation.source, relation.target].collect.with_index do |target, i|
      column_name = escaped(relation_field(target, i))
      Column.new(column_name, column_type(target.primary_key, target.primary_key_value))
    end
    cols << auto_increment_id_column()
  end

  def relation_fields(relation)
    [relation.source, relation.target].collect.with_index{|target, i| relation_field(target, i)}
  end

  def relation_field(target, index)
    target.type + "_" + target.primary_key + "#{index == 0 ? '_in' : '_out'}"
  end

  def relation_values(relation)
    [relation.source_id, relation.target_id]
  end

  def create_tables()
    all_tables.each do |table|
      run_stmt(create_table_stmt(table), /cannot use duplicate table name/) do
        table.columns.each do |column|
          run_stmt(add_column_stmt(table, column), /already exists|feature not supported/)
        end
      end
    end
  end

  def pk_field(node)
    node.primary_key
  end

  # runs a statement without any parameters. can be droppped immediately after execution
  def run_stmt(stmt, regexp = nil)
    begin
      run(stmt)
    rescue Exception => e
      if !regexp.nil? && !e.message.match(regexp)
        error("Statement failed: #{stmt}. Error: #{e.message}")
        raise e
      end
      yield if block_given?
    end
  end

  # overwrite if your data store supports statement preparation
  def prepare(query)
    query
  end

  # overwrite if you prepared your statement
  def drop(query)
    query
  end

  def execute(query, params)
    raise "implement how to run #{query} with params: #{params}"
  end

  def drop_statements()
    query_cache.each_pair do |node_type, stmt_hash|
      stmt_hash.each_pair{|fingerprint, stmt| drop(stmt)}
    end
  end

  def create_table_stmt(table)
    "CREATE TABLE #{table.name} (#{table.columns.collect{|col| column_definition(col)}.join(', ')} )"
  end

  def column_definition(column)
    "#{column.name} #{column.datatype} #{'PRIMARY KEY' if column.is_primary_key}"
  end

  def add_column_stmt(table, column)
    "ALTER TABLE #{table.name} ADD (#{column_definition(column)})"
  end

  def upsert_stmt(table_name, fields, pk_field)
    sql = "UPSERT #{table_name} (#{fields.collect{|f| escaped(f)}.join(", ")}) "
    sql << "VALUES (#{fields.collect{|f| "?"}.join(",")}) "
    sql << "WHERE #{escaped(pk_field)} = ?"
  end

  def insert_stmt(table_name, fields)
    sql = "INSERT INTO #{table_name} (#{fields.collect{|f| escaped(f)}.join(", ")}) "
    sql << "VALUES (#{fields.collect{|f| "?"}.join(",")}) "
  end

  def column_type(key, value)
    ttype = COLUMN_TYPES[value.class.name]
    ttype = "TEXT" if get_mapping(key)[:datatype] == "longText"
    ttype = "SHORTTEXT(5000)" if get_mapping(key)[:datatype] == "text"
    return ttype
  end

  def field_names(node)
    node.get_attributes.reject{|k,v| v.nil?}.collect{|attrib, value| column_name(attrib)}
  end

  def escaped(str)
    "\"#{column_name(str)}\""
  end

  def column_name(str)
    str.underscore.upcase
  end

  def auto_increment_id_column
    raise "return a column that creates an incrementing id value for added stuff"
  end

  def remove_duplicates
    load_sample()
    join_tables.each do |jt|
      info("Cleanup #{jt.name} with: #{cleanup_stmt(jt)}")
      run_query(cleanup_stmt(jt))
    end
  end

  def cleanup_stmt(jt)
    sql = "DELETE FROM #{jt.name} WHERE #{auto_increment_id_column.name} NOT IN "
    sql << "(SELECT MIN(#{auto_increment_id_column.name}) FROM #{jt.name} GROUP BY "
    sql << jt.columns.select{|c| c != auto_increment_id_column}.collect{|c| c.name}.join(", ") + ");"
  end

  class Table < Struct.new(:name, :columns)
  end

  class Column < Struct.new(:name, :datatype, :is_primary_key)
  end
end